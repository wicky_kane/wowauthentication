package group21.lvtn.wowauthentication.security;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import group21.lvtn.wowauthentication.R;
import group21.lvtn.wowauthentication.authentication.AuthenticationActivity;
import group21.lvtn.wowauthentication.authentication.FaceTrackerActivity;
import group21.lvtn.wowauthentication.camera.PhotoHandle;
import group21.lvtn.wowauthentication.feature_extraction.Eigenface;
import group21.lvtn.wowauthentication.feature_extraction.Fisherface;
import group21.lvtn.wowauthentication.feature_extraction.LBP;
import group21.lvtn.wowauthentication.utils.Constant;
import group21.lvtn.wowauthentication.utils.HelpMethod;
import group21.lvtn.wowauthentication.utils.InputHandle;
import group21.lvtn.wowauthentication.utils.StoreModel;

import static group21.lvtn.wowauthentication.camera.PreProcessImage.*;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import org.bytedeco.javacpp.opencv_core.Mat;

import java.io.File;
import java.nio.IntBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Vector;

public class PerformaceActivity extends ActionBarActivity {

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		/*if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id., new PlaceholderFragment()).commit();
		}*/
		//runTestResize();
		/*double[] eigenthreshold= {1000.f,1200.f, 1300.f, 1400.f, 1500.f, 1600.f, 1700.f, 1800.f, 1900.f, 2000.f};
		runEigenface(eigenthreshold, 0);
		runEigenface(eigenthreshold, 1);*/

		double threshold = 550.f;

		double[] fisherthreshold= new double[10];
		for (int i=0; i<10; i++) {
			fisherthreshold[i] = threshold + 50*i;
 		}
		//runFisherface(fisherthreshold, 0);
		runFisherface(fisherthreshold, 1);

		//double[] lbpthreshold= {100.f,150.f, 200.f, 250.f, 300.f, 350.f, 400.f, 450.f, 500.f, 600.f};
		//runLBP(lbpthreshold,0);
		//runLBP(lbpthreshold,1);
		//runCodebook();
		//runImageProcessing();
	}

	private void runImageProcessing(){
		Mat start1 = imread("/storage/emulated/0/Pictures/CameraAPIDemo/10-1.jpg");
		Mat start2 = imread("/storage/emulated/0/Pictures/CameraAPIDemo/10-2.jpg");
		Mat start3 = imread("/storage/emulated/0/Pictures/CameraAPIDemo/10-3.jpg");
		//Blur
		Mat start10 = BlurImage(start1);
		Mat start20 = BlurImage(start2);
		Mat start30 = BlurImage(start3);
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/10-1-blur.jpg",start10);
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/10-2-blur.jpg",start20);
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/10-3-blur.jpg", start30);
		//Contrast
		 start10 = ContrastImage(start1);
		 start20 = ContrastImage(start2);
		 start30 = ContrastImage(start3);
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/10-1-contrast.jpg",start10);
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/10-2-contrast.jpg", start20);
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/10-3-contrast.jpg",start30);

		Toast.makeText(getApplication(),"Writen", Toast.LENGTH_LONG);

	}

	private void runTestResize(){
		Mat start1 = imread("/storage/emulated/0/Pictures/CameraAPIDemo/1-1.jpg");

		Mat start10 = reSize(start1, 0.3,0.3);
		Log.e("SIZE", String.valueOf(start10.size()));
		imwrite("/storage/emulated/0/Pictures/CameraAPIDemo/1-1-resize.jpg", start10);

		Toast.makeText(getBaseContext(),"Writen", Toast.LENGTH_LONG);

	}

	private void runCodebook() {
		String pathSave =   PhotoHandle.getDir().getPath() + File.separator + "fisherface.xml";
		String projectSave =   PhotoHandle.getDir().getPath() + File.separator + "project_fisherface.xml";
		Fisherface fisher = StoreModel.readFisherface(pathSave, projectSave, 90, 1000);


	}

	private Mat readImage(int i, int j, int type) {
		final String EXTENSION = "-0.jpg";
		final String BINE = "-";
		final String JPG = ".jpg";
		final double SCALE = 0.3;

		String urlSignUp=PhotoHandle.getDir().getPath() + File.separator+"register"+File.separator;
		String urlTest=PhotoHandle.getDir().getPath() + File.separator+"test"+File.separator;

		Mat returnImage = new Mat();
		switch (type) {
			case 0:
				returnImage = ContrastImage(imread(urlSignUp + i + BINE + j + JPG));
				returnImage = reSize(returnImage, SCALE, SCALE);
				break;
			case 1:
				returnImage = ContrastImage(imread(urlTest + i + EXTENSION));
				returnImage = reSize(returnImage, SCALE, SCALE);
				break;
			default:
				break;
		}
		return returnImage;
	}

	private Vector<Mat>  readSignUp(){
		Vector<Mat> signUpVector = new Vector<Mat>();
		final int TYPE_SIGNUP = 0;
		for(int i = 1; i<= 100 ; i++){
			for(int j = 1; j<= 5; j++) {
				Mat signUpImage = readImage(i,j,TYPE_SIGNUP);
				signUpVector.add(signUpImage);
			}
		}
		return signUpVector;
	}

	private void readTest(Vector<Mat> rightTestVector, Vector<Mat> wrongTestVector) {
		final int TYPE_TEST = 1;
		for(int i = 1; i <= 50; i++ ){
			Mat rightTestImage = readImage(i, 0, TYPE_TEST);
			rightTestVector.add(rightTestImage);

			Mat wrongTestImage = readImage(i + 50, 0, TYPE_TEST);;
			wrongTestVector.add(wrongTestImage);
		}
	}

	private int processTest(int type, Eigenface eigen, Vector<Mat> testVector,Vector<Mat> signUpVector, int index){
	//type = 0 => right test basic
	//type = 1 => wrong test basic
	//type = 2 => right test codebook
	//type = 4 => wrong test codebook

		int frr = 0, frr_codebook = 0;
		int far = 0, far_codebook = 0;
		int imagePerOne = 5;

		for (Mat image : testVector) {
			Vector<Mat> project = new Vector<Mat>();
			for (int i = index; i < index + imagePerOne; i++) {
				project.add(signUpVector.get(i));
			}

			index += imagePerOne;

			//Sign up
			eigen.doProject(project);
			//Test basic
			if (type == 0 || type == 1) {
				int result = eigen.predictEigenface(image);
				Log.e("RESULT basic " + index / 5 + ": ", String.valueOf(result));
				if (type == 0 && result != 1) frr++;
				if (type == 1 && result == 1) far++;
			} else {
			//Test codebook
				int DELTA =25;
				Vector<Vector> sketch = Codebook.codebookGenerator(eigen._projection, DELTA);
				Vector<Vector> hash = new Vector<Vector>();
				try {
					hash = HelpMethod.hashTemplateOri(eigen._projection);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				Mat authenMat = eigen.getAuthenMat(image);
				Vector<Vector> recover = Codebook.codebookRecover(authenMat, sketch);

				Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
				try {
					sketchHash = HelpMethod.hashTemplate(recover);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				int result_codebook = Codebook.predict(sketchHash, hash, Constant.EIGENGFACE, 0);

				Log.e("RESULT codebook " + index / 5 + ": ", String.valueOf(result_codebook));
				if (type == 2 && result_codebook != 1) frr_codebook++;
				if (type == 3 && result_codebook == 1) far_codebook++;
			}


		}

		switch (type) {
			case 0: return frr;
			case 1: return far;
			case 2: return frr_codebook;
			case 3: return far_codebook;
			default: return 50;
		}
	}

	private void runTest(int type, double[] listThreshold, Eigenface eigen, Vector<Mat> signUpVector, Vector<Mat> rightTestVector, Vector<Mat> wrongTestVector) {
		//Type = 0 => Basic
		//Type = 1 => Codebook
		final int BASIC = 0;
		final int CODEBOOK = 1;

		final int RIGHTTEST_BASIC = 0;
		final int WRONGTEST_BASIC = 1;

		final int RIGHTTEST_CB = 2;
		final int WRONGTEST_CB = 3;

		if (type == BASIC) {
			//Basic
			for (double threshold : listThreshold) {

				eigen._threshold = threshold;
				Log.e("THRESHOLD : ", String.valueOf(threshold));

				//FRR /50
				int frr = processTest(RIGHTTEST_BASIC, eigen, rightTestVector, signUpVector, 0);
				Log.e("FRR basic" + threshold + ": ", frr + "/50");

				//FAR /50
				int far = processTest(WRONGTEST_BASIC, eigen, wrongTestVector, signUpVector, 50);

				Log.e("FAR basic "+ threshold+": ", far + "/50");

			}

		}
		else {
			//Codebook
			//FRR /50
			int frr = processTest(RIGHTTEST_CB, eigen, rightTestVector, signUpVector, 0);
			Log.e("FRR codebook: ", frr + "/50");

			//FAR /50
			int far = processTest(WRONGTEST_CB, eigen, wrongTestVector, signUpVector, 50);

			Log.e("FAR codebook : ", far + "/50");
		}
	}


	private void runEigenface(double[] eigenThreshold, int type){
		//Read input - Ko tinh thoi gian read input
		InputHandle input = new InputHandle(0); //Da sort theo ten

		Vector<Mat> signUpVector = readSignUp();
		Vector<Mat> rightTestVector = new Vector<Mat>();
		Vector<Mat> wrongTestVector = new Vector<Mat>();
		readTest(rightTestVector, wrongTestVector);

		// Set time start here
		Eigenface eigen = new Eigenface(input.getSrc(), input.getLables(), 30, eigenThreshold[0]);

		runTest(type, eigenThreshold,eigen,signUpVector,rightTestVector, wrongTestVector);

		Toast.makeText(getBaseContext(),"Run Eigenface completely!", Toast.LENGTH_LONG).show();
	}

	/**
	 * RUN ALL
	 * Combine EIGEN - FISHER _LBP
	 *
	 */

	private int processTestAll(int type, Eigenface eigen, Fisherface fisher, LBP lbp, double lbpthreshold, Vector<Mat> testVector,Vector<Mat> signUpVector, int index){
		//type = 0 => right test basic
		//type = 1 => wrong test basic
		//type = 2 => right test codebook
		//type = 4 => wrong test codebook

		int frr = 0, frr_codebook = 0;
		int far = 0, far_codebook = 0;
		int imagePerOne = 5;

		for (Mat image : testVector) {
			Vector<Mat> project = new Vector<Mat>();
			for (int i = index; i < index + imagePerOne; i++) {
				project.add(signUpVector.get(i));
			}

			index += imagePerOne;

			//Sign up
			eigen.doProject(project);
			fisher.doProject(project);
			LBP lbpSignup = new LBP(project);
			//convert int[][] to MaT
			Vector<Mat> list_mat_hist = new Vector<>(lbpSignup.list_hist.size());
			for (int n=0;n<lbpSignup.list_hist.size();n++){
				Mat hist =  new Mat(256, 1, CV_32SC1);
				IntBuffer histBuf = hist.createBuffer();
				for (int i=0;i<256;i++){
					histBuf.put(i, lbpSignup.list_hist.get(n)[i]);
				}
				list_mat_hist.add(hist);
			}

			//Test basic
			if (type == 0 || type == 1) {

				//Eigenface
				boolean result1 = (eigen.predictEigenface(image)==1);
				Log.e("RESULT basic " + index / 5 + " - eigenface: ", String.valueOf(result1));

				//Fisherface
				boolean result2 = (fisher.predict(image)==1);
				Log.e("RESULT basic " + index / 5 + " - fisherface: ", String.valueOf(result2));

				//LBP
				boolean result3 = (lbpSignup.predictPerformance(image,lbpthreshold) == 1);
				//Log.e("RESULT basic " + index / 5 + ": ", String.valueOf(result));
				boolean result = result2 && (result1 || result3);

				if (type == 0 && !result) frr++;
				if (type == 1 && result) far++;

			} else {
				//Test codebook
				/**
				 * EIGENFACE
				 */
				int DELTA =25;
				Vector<Vector> sketch = Codebook.codebookGenerator(eigen._projection, DELTA);
				Vector<Vector> hash = new Vector<Vector>();
				try {
					hash = HelpMethod.hashTemplateOri(eigen._projection);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				Mat authenMat = eigen.getAuthenMat(image);
				Vector<Vector> recover = Codebook.codebookRecover(authenMat, sketch);

				Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
				try {
					sketchHash = HelpMethod.hashTemplate(recover);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				boolean result_codebook_eigen = Codebook.predict(sketchHash, hash, Constant.EIGENGFACE, 0) == 1;

				Log.e("RESULT codebook " + index / 5 + ": ", String.valueOf(result_codebook_eigen));


				/**
				 * FISHERFACE
				 */
				 DELTA = 25;
				sketch = Codebook.codebookGenerator(fisher._projection,DELTA);
				 hash = new Vector<Vector>();
				try {
					hash = HelpMethod.hashTemplateOri(fisher._projection);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				authenMat = fisher.getAuthenMat(image);
				recover = Codebook.codebookRecover(authenMat, sketch);

				sketchHash = new Vector<Vector>(recover.size());
				try {
					sketchHash = HelpMethod.hashTemplate(recover);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				boolean result_codebook_fisher = Codebook.predict(sketchHash, hash, Constant.FISHERFACE,0) == 1;
				Log.e("RESULT codebook " + index / 5 + ": ", String.valueOf(result_codebook_fisher));

				/**
				 * LBP
				 */
				DELTA = 25;
				sketch = Codebook.codebookGenerator(list_mat_hist,DELTA);
				hash = new Vector<Vector>();
				try {
					hash = HelpMethod.hashTemplateOri(list_mat_hist);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				//Get hist from input to recover
				Vector<Mat> inputMat = new Vector<>(1);;
				inputMat.add(image);
				LBP lbpauthen = new LBP(inputMat, null);

				authenMat =  new Mat(256, 1, CV_32SC1);
				IntBuffer histBuf = authenMat.createBuffer();
				for (int i=0;i<256;i++){
					histBuf.put(i, lbpauthen.list_hist.get(0)[i]);
				}
				recover = Codebook.codebookRecover(authenMat, sketch);

				sketchHash = new Vector<Vector>(recover.size());
				try {
					sketchHash = HelpMethod.hashTemplate(recover);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				boolean result_codebook_lbp = Codebook.predict(sketchHash, hash, Constant.LBPH,0) == 1;

				boolean result_codebook = result_codebook_fisher && (result_codebook_eigen || result_codebook_lbp);
				//Log.e("RESULT codebook " + index / 5 + ": ", String.valueOf(result_codebook));
				if (type == 2 && !result_codebook) frr_codebook++;
				if (type == 3 && result_codebook) far_codebook++;
			}


		}

		switch (type) {
			case 0: return frr;
			case 1: return far;
			case 2: return frr_codebook;
			case 3: return far_codebook;
			default: return 50;
		}
	}

	private void runTestAll(int type, double eigenThreshold, Eigenface eigen,
							double fisherThreshold, Fisherface fisher,
							double listThreshold, LBP lbp,
							Vector<Mat> signUpVector, Vector<Mat> rightTestVector, Vector<Mat> wrongTestVector) {
		//Type = 0 => Basic
		//Type = 1 => Codebook
		final int BASIC = 0;
		final int CODEBOOK = 1;

		final int RIGHTTEST_BASIC = 0;
		final int WRONGTEST_BASIC = 1;

		final int RIGHTTEST_CB = 2;
		final int WRONGTEST_CB = 3;

		if (type == BASIC) {
			//Basic

				eigen._threshold = eigenThreshold;
				Log.e("THRESHOLD"," EIGEN : " + String.valueOf(eigenThreshold));

				fisher._threshold = fisherThreshold;
				Log.e("THRESHOLD"," FISHER : " + String.valueOf(fisherThreshold));

				//FRR /50
				int frr = processTestAll(RIGHTTEST_BASIC, eigen, fisher, lbp, listThreshold, rightTestVector, signUpVector, 0);
				Log.e("FRR basic " + "ALL" + ": ", frr + "/50");

				//FAR /50
				int far = processTestAll(WRONGTEST_BASIC, eigen, fisher, lbp, listThreshold, wrongTestVector, signUpVector, 50);

				Log.e("FAR basic " + "ALL" + ": ", far + "/50");



		}
		else {
			//Codebook
			//FRR /50
			int frr = processTestAll(RIGHTTEST_CB, eigen, fisher, lbp, listThreshold, rightTestVector, signUpVector, 0);
			Log.e("FRR codebook: ", "ALL : " + frr + "/50");

			//FAR /50
			int far = processTestAll(WRONGTEST_CB, eigen, fisher, lbp, listThreshold, wrongTestVector, signUpVector, 50);

			Log.e("FAR codebook : ",  "ALL : " + far + "/50");
		}
	}

	private void runAll(double eigenThreshold,double fisherThreshold,double listThreshold, int type){
		//Read input - Ko tinh thoi gian read input
		InputHandle input = new InputHandle(0); //Da sort theo ten

		Vector<Mat> signUpVector = readSignUp();
		Vector<Mat> rightTestVector = new Vector<Mat>();
		Vector<Mat> wrongTestVector = new Vector<Mat>();
		readTest(rightTestVector, wrongTestVector);

		// Set time start here
		Eigenface eigen = new Eigenface(input.getSrc(), input.getLables(), 30, eigenThreshold);
		Fisherface fisher = new Fisherface(input.getSrc(), input.getLables(),
				input.getNumber_of_classes(), 30, fisherThreshold);
		LBP lbp = new LBP(signUpVector);

		runTestAll(type, eigenThreshold, eigen, fisherThreshold, fisher, listThreshold, lbp, signUpVector, rightTestVector, wrongTestVector);

		Toast.makeText(getBaseContext(),"Run ALL completely!", Toast.LENGTH_LONG).show();
	}

	///////////////////////////////////
	//////////////////////////////////
	//////////////////////////////////FISHER FACE
	private int processTestFisher(int type, Fisherface fisher, Vector<Mat> testVector,Vector<Mat> signUpVector, int index){
		//type = 0 => right test basic
		//type = 1 => wrong test basic
		//type = 2 => right test codebook
		//type = 4 => wrong test codebook

		int frr = 0, frr_codebook = 0;
		int far = 0, far_codebook = 0;
		int imagePerOne = 5;

		for (Mat image : testVector) {
			Vector<Mat> project = new Vector<Mat>();
			for (int i = index; i < index + imagePerOne; i++) {
				project.add(signUpVector.get(i));
			}

			index += imagePerOne;

			//Sign up
			fisher.doProject(project);
			//Test basic
			if (type == 0 || type == 1) {
				int result = fisher.predict(image);
				Log.e("RESULT basic " + index / 5 + ": ", String.valueOf(result));
				if (type == 0 && result != 1) frr++;
				if (type == 1 && result == 1) far++;
			} else {
				//Test codebook
				int DELTA = 25;
				Vector<Vector> sketch = Codebook.codebookGenerator(fisher._projection,DELTA);
				Vector<Vector> hash = new Vector<Vector>();
				try {
					hash = HelpMethod.hashTemplateOri(fisher._projection);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				Mat authenMat = fisher.getAuthenMat(image);
				Vector<Vector> recover = Codebook.codebookRecover(authenMat, sketch);

				Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
				try {
					sketchHash = HelpMethod.hashTemplate(recover);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				int result_codebook = Codebook.predict(sketchHash, hash, Constant.FISHERFACE,0);

				Log.e("RESULT codebook " + index / 5 + ": ", String.valueOf(result_codebook));
				if (type == 2 && result_codebook != 1) frr_codebook++;
				if (type == 3 && result_codebook == 1) far_codebook++;
			}


		}

		switch (type) {
			case 0: return frr;
			case 1: return far;
			case 2: return frr_codebook;
			case 3: return far_codebook;
			default: return 50;
		}
	}


	private void runTestFisher(int type, double[] listThreshold, Fisherface fisher,
							   Vector<Mat> signUpVector, Vector<Mat> rightTestVector, Vector<Mat> wrongTestVector) {
		//Type = 0 => Basic
		//Type = 1 => Codebook
		final int BASIC = 0;
		final int CODEBOOK = 1;

		final int RIGHTTEST_BASIC = 0;
		final int WRONGTEST_BASIC = 1;

		final int RIGHTTEST_CB = 2;
		final int WRONGTEST_CB = 3;

		if (type == BASIC) {
			//Basic
			for (double threshold : listThreshold) {

				fisher._threshold = threshold;
				Log.e("THRESHOLD : ", String.valueOf(threshold));

				//FRR /50
				int frr = processTestFisher(RIGHTTEST_BASIC, fisher, rightTestVector, signUpVector, 0);
				Log.e("NEED FRR basic" + threshold + ": ", frr + "/50");

				//FAR /50
				int far = processTestFisher(WRONGTEST_BASIC, fisher, wrongTestVector, signUpVector, 50);

				Log.e("NEED FAR basic "+ threshold+": ", far + "/50");

			}

		}
		else {
			//Codebook
			//FRR /50
			int frr = processTestFisher(RIGHTTEST_CB, fisher, rightTestVector, signUpVector, 0);
			Log.e("NEED FRR codebook: ", frr + "/50");

			//FAR /50
			int far = processTestFisher(WRONGTEST_CB, fisher, wrongTestVector, signUpVector, 50);

			Log.e("NEED FAR codebook : ", far + "/50");
		}
	}

	private void runFisherface(double[] fisherThreshold, int type) {
		//Read input - Ko tinh thoi gian read input
		InputHandle input = new InputHandle(0); //Da sort theo ten

		Vector<Mat> signUpVector = readSignUp();
		Vector<Mat> rightTestVector = new Vector<Mat>();
		Vector<Mat> wrongTestVector = new Vector<Mat>();
		readTest(rightTestVector, wrongTestVector);

		// Set time start here
		Fisherface fisher = new Fisherface(input.getSrc(), input.getLables(),
				input.getNumber_of_classes(), 30, fisherThreshold[0]);

		runTestFisher(type, fisherThreshold,fisher,signUpVector,rightTestVector, wrongTestVector);

		Toast.makeText(getBaseContext(),"Run Fisherface completely!", Toast.LENGTH_LONG).show();
	}

	//far/50 frr/50
	private int processTestLBP(int type, Vector<Mat> signUpVector, Vector<Mat> testVector, int index, double threshold){
		//type = 0 => right test basic
		//type = 1 => wrong test basic
		//type = 2 => right test codebook
		//type = 4 => wrong test codebook

		int frr = 0, frr_codebook = 0;
		int far = 0, far_codebook = 0;
		int imagePerOne = 5;

		for (Mat image : testVector) {
			Vector<Mat> project = new Vector<Mat>();
			for (int i = index; i < index + imagePerOne; i++) {
				project.add(signUpVector.get(i));
			}

			index += imagePerOne;

			//Sign up
			LBP lbpSignup = new LBP(project);
			//convert int[][] to MaT
			Vector<Mat> list_mat_hist = new Vector<>(lbpSignup.list_hist.size());
			for (int n=0;n<lbpSignup.list_hist.size();n++){
				Mat hist =  new Mat(256, 1, CV_32SC1);
				IntBuffer histBuf = hist.createBuffer();
				for (int i=0;i<256;i++){
					histBuf.put(i, lbpSignup.list_hist.get(n)[i]);
				}
				list_mat_hist.add(hist);
			}

			//Test basic
			if (type == 0 || type == 1) {
				int result = lbpSignup.predictPerformance(image,threshold);
				//Log.e("RESULT basic " + index / 5 + ": ", String.valueOf(result));
				if (type == 0 && result != 1) frr++;
				if (type == 1 && result == 1) far++;
			} else {
				//Test codebook
				int DELTA = 25;
				Vector<Vector> sketch = Codebook.codebookGenerator(list_mat_hist,DELTA);
				Vector<Vector> hash = new Vector<Vector>();
				try {
					hash = HelpMethod.hashTemplateOri(list_mat_hist);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				//Get hist from input to recover
				Vector<Mat> inputMat = new Vector<>(1);;
				inputMat.add(image);
				LBP lbpauthen = new LBP(inputMat, null);

				Mat authenMat =  new Mat(256, 1, CV_32SC1);
				IntBuffer histBuf = authenMat.createBuffer();
				for (int i=0;i<256;i++){
					histBuf.put(i, lbpauthen.list_hist.get(0)[i]);
				}
				Vector<Vector> recover = Codebook.codebookRecover(authenMat, sketch);

				Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
				try {
					sketchHash = HelpMethod.hashTemplate(recover);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (SignatureException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				}

				int result_codebook = Codebook.predict(sketchHash, hash, Constant.LBPH,0);

				//Log.e("RESULT codebook " + index / 5 + ": ", String.valueOf(result_codebook));
				if (type == 2 && result_codebook != 1) frr_codebook++;
				if (type == 3 && result_codebook == 1) far_codebook++;
			}


		}

		switch (type) {
			case 0: return frr;
			case 1: return far;
			case 2: return frr_codebook;
			case 3: return far_codebook;
			default: return 50;
		}
	}

	private void runTestLBP(int type, double[] listThreshold, Vector<Mat> signUpVector, Vector<Mat> rightTestVector, Vector<Mat> wrongTestVector) {
		//Type = 0 => Basic
		//Type = 1 => Codebook
		final int BASIC = 0;
		final int CODEBOOK = 1;

		final int RIGHTTEST_BASIC = 0;
		final int WRONGTEST_BASIC = 1;

		final int RIGHTTEST_CB = 2;
		final int WRONGTEST_CB = 3;

		if (type == BASIC) {
			//Basic
			for (double threshold : listThreshold) {
				//FRR /50
				int frr = processTestLBP(RIGHTTEST_BASIC, signUpVector, rightTestVector, 0,threshold);
				Log.e("FRR basic" + threshold + ": ", frr + "/50");

				//FAR /50
				int far = processTestLBP(WRONGTEST_BASIC, signUpVector, wrongTestVector, 50,threshold);
				Log.e("FAR basic "+ threshold+": ", far + "/50");
			}

		}
		else {
			//Codebook
			//FRR /50
			double NULLTHRESHOLD = 0;
			int frr = processTestLBP(RIGHTTEST_CB, signUpVector, rightTestVector, 0, NULLTHRESHOLD);
			Log.e("FRR codebook: ", frr + "/50");

			//FAR /50
			int far = processTestLBP(WRONGTEST_CB, signUpVector, wrongTestVector , 50, NULLTHRESHOLD);

			Log.e("FAR codebook : ", far + "/50");
		}
	}

	private void runLBP(double[] listThreshold, int type){
		//Read input - Ko tinh thoi gian read input
		InputHandle input = new InputHandle(0); //Da sort theo ten

		Vector<Mat> signUpVector = readSignUp();
		Vector<Mat> rightTestVector = new Vector<Mat>();
		Vector<Mat> wrongTestVector = new Vector<Mat>();
		readTest(rightTestVector, wrongTestVector);

		// Set time start here
		//Log.e("START LBP!!!","Start LBP");
		LBP lbp = new LBP(signUpVector);
		runTestLBP(type,listThreshold,signUpVector,rightTestVector,wrongTestVector);
		//runTestLBP(1,listThreshold,lbp,rightTestVector,wrongTestVector);
		//Log.e("END LBP!!!","End LBP");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
}
