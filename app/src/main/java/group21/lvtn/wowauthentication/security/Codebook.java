package group21.lvtn.wowauthentication.security;

import android.graphics.Matrix;
import android.util.Log;

import org.bytedeco.javacpp.indexer.DoubleIndexer;
import static org.bytedeco.javacpp.opencv_core.*;

import org.bytedeco.javacpp.indexer.Indexable;
import org.bytedeco.javacpp.indexer.IntBufferIndexer;
import org.bytedeco.javacpp.indexer.IntIndexer;
import org.bytedeco.javacpp.indexer.UByteBufferIndexer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.FrameGrabber;

import java.lang.reflect.Array;
import java.util.Vector;

import group21.lvtn.wowauthentication.utils.Constant;


/**
 * Created by Lotso Chik on 10/26/2016.
 */
public class Codebook {
    /**
     * w: feature template
     * N: number of elements of w
     * S: number of codewords in codebook
     * delta: distance between 2 codewords is 2*delta
     */

    //Range voi Delta lay trong VD cua thay Khanh
    private static final int MAX_RANGE = 1000;
    public static final int MAX_RANGE_2 = 500; //range la 1000, nhung range hien tai dang la [0,2]
    private static int DELTA = 25;

    private static Vector<Integer> template;
    private static Vector<Integer> sketch;
    private static Vector<Integer> recovery;

    public Codebook(Mat w,int del) {
        try {
            DoubleIndexer idx = w.createIndexer();
            normalize(w, w);
        } catch (Exception e){
            //normalize(w, w);
        }
        this.template = Mat2Vector(w);
        this.DELTA = del;
    }

    public Codebook(Mat w, Vector<Integer> sketch) {
        //Template luc nay la authentication template
        try {
            DoubleIndexer idx = w.createIndexer();
            normalize(w, w);
        } catch (Exception e){
            //normalize(w, w);
        }
        this.template = Mat2Vector(w);
        this.sketch = sketch;
    }

    public void sketchGeneration() {
        // si = wi - M(wi)
        sketch = new Vector<Integer>(template.size());
        String ske="";
        int maxvalue = 0;
        int numOfCodework = 0;

        for (int i=0; i<template.size(); i++) {
            int index = Math.round((float)template.get(i) / (2*DELTA));
            int codeword = index*2*DELTA;
            int ss = template.get(i) - codeword;
            sketch.add(ss);
              if (index>numOfCodework) numOfCodework= index;
            if (template.get(i)>maxvalue) maxvalue = template.get(i);
            ske+="-"+sketch.get(i);
        }
        Log.e("Number of cw","max value= "+ String.valueOf(maxvalue) +"/max cw "+ String.valueOf(numOfCodework));
        Log.e("sketch is: ",ske);
    }

    public void featureRecovery() {
        // w'i = M(zi - si) + si
        recovery = new Vector<Integer>(template.size());
        for (int i=0; i<template.size(); i++) {
            int index = Math.round((float) (template.get(i) - sketch.get(i)) / (2 * DELTA));
            int codeword = index * 2 * DELTA;
            int reco = codeword + sketch.get(i);
            recovery.add(reco);
        }
    }

    public static Vector<Integer> getSketch() {
        return sketch;
    }

    public static Vector<Integer> getRecovery() {
        return recovery;
    }

    public  Vector<Integer> getTemplate() {
        return template;
    }

    public static Vector<Integer> Mat2Vector (Mat m) {
        Vector<Integer> result = new Vector<Integer>(m.cols());
        try {
            DoubleIndexer idx = m.createIndexer();
            for (int i = 0; i < m.cols(); i++) {
                result.add(quantization(idx.get(0, i)));
            }
        } catch (Exception e) {
            IntBufferIndexer idx = m.createIndexer();

            for (int i = 0; i < m.rows(); i++) {
                result.add(idx.get(0, i));
            }
        }
        return result;
    }

    private static Integer quantization (double d) {
        int temp = (int) ((d+1)*MAX_RANGE_2); //chuyen range tu [-1, 1] thanh [0, 2]
        Integer result = temp;
        return result;
    }

    public static int predict(Vector<Vector> sketchHash, Vector<Vector> projectHash, String algorithm, float threshold) {
        int minClass = -1;
        int sameElement = -1;

        /*int threshold = sketchHash.get(0).size()/2;

        switch (algorithm){
            case Constant.EIGENGFACE:
                threshold = sketchHash.get(0).size()*2/3;
                break;
            case Constant.FISHERFACE:

                break;
            case Constant.LBPH:
                threshold = sketchHash.get(0).size()*3/4;
                break;
            case Constant.ALL:

                break;
            default: break;
        }*/


        for (int i=0; i<sketchHash.size(); i++) {

            Vector<String> sketch_i = sketchHash.get(i);
            Vector<String> project_i = projectHash.get(i);

            Log.e("PROJECT", i + " " + project_i);
            Log.e("RECOVER", i + " " + sketch_i);
            Log.e("THRESHOLD"," " + threshold );

            int count = 0;
            for (int j=0; j<sketch_i.size(); j++) {
                if (sketch_i.get(j).equals(project_i.get(j)))
                    count++;
            }

            Log.e("COUNT "+i+":", count + " ");

            if ((count > sameElement) && (count >= threshold)) {
                sameElement = count;
                minClass = 1;
                break;
            }
        }
        ////Log.e("Files", "Result " + String.valueOf(minClass));
        return minClass;
    }


    public static Vector<Vector> codebookRecover(opencv_core.Mat authenTemplate, Vector<Vector> sketch) {
        Vector<Vector> recover = new Vector<>(sketch.size());

        for (int i=0; i<sketch.size(); i++) {
            Codebook cb = new Codebook(authenTemplate, sketch.get(i));
            cb.featureRecovery();
            recover.add(cb.getRecovery());
        }
        return recover;
    }

    public static Vector<Vector> codebookGenerator(Vector<opencv_core.Mat> projection, int DELTA) {
        Vector<Vector> sketch = new Vector<>(projection.size());
        for (int i = 0; i < projection.size(); i++) {
            Codebook cb = new Codebook(projection.get(i),DELTA);
            cb.sketchGeneration();
            sketch.add(cb.getSketch());
            //Vector template
//            Log.e("TEST", "Vector ori "+i+" : " + cb.getTemplate().toString());
        }
        return sketch;
    }
}
