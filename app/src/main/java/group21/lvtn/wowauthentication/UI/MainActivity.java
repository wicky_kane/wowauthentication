package group21.lvtn.wowauthentication.UI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.Toast;

import java.util.prefs.Preferences;

import cn.pedant.SweetAlert.*;
import group21.lvtn.wowauthentication.R;
import group21.lvtn.wowauthentication.authentication.AuthenticationActivity;
import group21.lvtn.wowauthentication.authentication.FaceTrackerActivity;
import group21.lvtn.wowauthentication.lockscreenservice.LockScreenService;
import group21.lvtn.wowauthentication.settings.PrefManager;
import group21.lvtn.wowauthentication.settings.SettingsActivity;
import group21.lvtn.wowauthentication.utils.Constant;
import group21.lvtn.wowauthentication.utils.HelpMethod;

@SuppressLint("NewApi")
public class MainActivity extends AppCompatActivity {
 
    /***********Layout******************/
    private Button btnSignup;
    private Button btnAuthen;
    private Switch mySwitch;
    /**********Setting******************/
    private PrefManager prefManager;
    /**********Dialog******************/
    private SweetAlertDialog pDialog;
    /************ Code Request*********/
    static final int SIGNUP_REQUEST = 1;

    // Set multi index, for lower android version
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        //Set full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(group21.lvtn.wowauthentication.R.layout.activity_signup);

        // Making notification bar transparent
        changeStatusBarColor();

        prefManager = new PrefManager(this);
        btnAuthen = (Button) findViewById(R.id.btn_authentication);
        btnSignup = (Button) findViewById(R.id.btn_singup);
        mySwitch = (Switch) findViewById(R.id.mySwitch);
        //Check service
        //if(!prefManager.isSignedUp())
          //  HelpMethod.serviceProcess(getApplication());

        //Button SignUp handle
        btnSignup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //If already Signed up, alert, if not open camera to sign up
                //prefManager.setSignedUp(true);
                if (!prefManager.isSignedUp()) {
                    conFirmReSignUp();
                }
                else {
                        //Xoa image
                        HelpMethod.deleteTempImages();
                        launchFaceTrackerActivity();
                        // System.exit(0);
                        finish();
                }
            }
        });
        //Button Authen handle
        btnAuthen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAuthenticationActivity();
            }
        });

        btnAuthen.setVisibility(View.INVISIBLE);
        mySwitch.setVisibility(View.INVISIBLE);
        if(prefManager.isActive()) mySwitch.setChecked(true); else mySwitch.setChecked(false);
        //Active or deactive lock screen
        mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                // TODO Auto-generated method stub
				 if(arg1){
                     // If not signed up, alert message
					 if(prefManager.isSignedUp()){
						 	HelpMethod.showMessage(Constant.SIGN_UP_INFO, MainActivity.this);
						 	mySwitch.setChecked(false);
				    		mySwitch.setText(mySwitch.getText());
				    		return;
					 }
                     prefManager.setLockScreen(true);
                     //Start service Lock Screen
                     runLockScreenService();
				    }else{
                     //Stop service Lock Screen
                     prefManager.setLockScreen(false);
                     stopLockScreenService();
                 }
			}
		});
       
    }

    private void runLockScreenService(){
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, LockScreenService.class);
        startService(intent);
        Toast.makeText(getBaseContext(), "Active Lock Screen", Toast.LENGTH_SHORT).show();
    }

    private void stopLockScreenService(){
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, LockScreenService.class);
        stopService(intent);
        Toast.makeText(getBaseContext(),"Deactive Lock Screen", Toast.LENGTH_SHORT).show();
    }
    private void launchFaceTrackerActivity() {
        startActivity(new Intent(this, FaceTrackerActivity.class));
    }

    private void launchAuthenticationActivity() {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }

    private void conFirmReSignUp(){
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Do you want to re-signup?")
                .setContentText("You have already signed up!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                doReSignUp();
            }
        })
                .show();
    }

    private void doReSignUp() {
        Intent intent = new Intent(this, AuthenticationActivity.class);
        startActivityForResult(intent, SIGNUP_REQUEST);
    }

    /**
     * Making notification bar transparent
     */

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (SIGNUP_REQUEST) : {
                if (resultCode == Activity.RESULT_OK) {
                    if(pDialog != null) pDialog.cancel();
                    launchFaceTrackerActivity();
                }
                break;
            }
          default:
                break;
            }
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_help) {
            prefManager.setFirstTimeLaunch(true);
            startActivity(new Intent(this, WelcomeActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
