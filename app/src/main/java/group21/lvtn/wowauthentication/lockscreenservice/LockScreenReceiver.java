package group21.lvtn.wowauthentication.lockscreenservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * Lock Screen Receiver
 */
public class LockScreenReceiver extends BroadcastReceiver {
    public static final String LOCK_SCREEN_ACTION = "android.intent.lockscreen";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Intent lockIntent = new Intent(LOCK_SCREEN_ACTION);
            lockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

            context.startActivity(lockIntent);
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {

        }
    }
}
