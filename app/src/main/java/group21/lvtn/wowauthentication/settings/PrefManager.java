package group21.lvtn.wowauthentication.settings;

import android.content.Context;
import android.content.SharedPreferences;
 
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
 
    // shared pref mode
    int PRIVATE_MODE = 0;
 
    // Shared preferences file name
    private static final String PREF_NAME = "wow-welcome";
 
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_SIGN_UP = "IsSignUp";
    private static final String IS_COPY = "IsCopy";
    private static final String IS_ACTIVE = "IsAvtiveLockScreen";
    private static final String IS_SS = "IsSS";
 
    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
 
    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }
    
    public void setSignedUp(boolean isSignUp) {
        editor.putBoolean(IS_SIGN_UP, isSignUp);
        editor.commit();
    }

    public void setCopy(boolean IsCopy) {
        editor.putBoolean(IS_COPY, IsCopy);
        editor.commit();
    }

    public void setLockScreen(boolean IsActive) {
        editor.putBoolean(IS_ACTIVE, IsActive);
        editor.commit();
    }

    public void setSecurity(boolean isSS) {
        editor.putBoolean(IS_SS, isSS);
        editor.commit();
    }
 
    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean isSignedUp() {
        return pref.getBoolean(IS_SIGN_UP, true);
    }

    public boolean isCopy() {
        return pref.getBoolean(IS_COPY, true);
    }

    public boolean isActive() {
        return pref.getBoolean(IS_ACTIVE, true);
    }

    public boolean isSecurity() {
        return pref.getBoolean(IS_SS, true);
    }
 
}