package group21.lvtn.wowauthentication.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.bytedeco.javacpp.opencv_core;

import java.io.File;
import java.io.FilenameFilter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Vector;

import cn.pedant.SweetAlert.SweetAlertDialog;
import group21.lvtn.wowauthentication.authentication.FaceTrackerActivity;
import group21.lvtn.wowauthentication.camera.PhotoHandle;
import group21.lvtn.wowauthentication.lockscreenservice.LockScreenService;
import group21.lvtn.wowauthentication.security.Codebook;
import group21.lvtn.wowauthentication.security.HmacSha1Signature;

/**
 * Created by wicky on 11/29/2016.
 */
public class HelpMethod {
    public static void showMessage(String msg, Context context){
        SweetAlertDialog sd = new SweetAlertDialog(context);
        sd.setCancelable(true);
        sd.setCanceledOnTouchOutside(true);
        sd.setTitleText(msg);
        sd.show();
    }

    public static Vector<Vector> hashTemplate(Vector<Vector> sketch) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        Vector<Vector> hash = new Vector<>(sketch.size());

        HmacSha1Signature Hmac = new HmacSha1Signature();

        for (int i = 0; i < sketch.size(); i++) {
            Vector<Integer> s = sketch.elementAt(i);
            Vector<String> s_hash = new Vector(s.size());

            for (int j = 0; j < s.size(); j++) {
                s_hash.add(Hmac.calculateRFC2104HMAC(String.valueOf(s.get(j)), FaceTrackerActivity.HMAC_KEY));
            }
            hash.add(s_hash);
        }
        return hash;
    }

    public static Vector<Vector> hashTemplateOri(Vector<opencv_core.Mat> sketch) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        Vector<Vector> hash = new Vector<>(sketch.size());

        HmacSha1Signature Hmac = new HmacSha1Signature();

        for (int i = 0; i < sketch.size(); i++) {
            Vector<Integer> s = Codebook.Mat2Vector(sketch.get(i));
            Vector<String> s_hash = new Vector(s.size());

            ////Log.e("VECTOR ORI", i + " " + s);
            for (int j = 0; j < s.size(); j++) {
//                //Log.e("vector element", j + " " + s.get(j) + " " +
//                        Hmac.calculateRFC2104HMAC(String.valueOf(s.get(j)), HMAC_KEY));
                s_hash.add(Hmac.calculateRFC2104HMAC(String.valueOf(s.get(j)), FaceTrackerActivity.HMAC_KEY));
            }
            //Log.e("VECTOR HASH", i + " " + s_hash);
            hash.add(s_hash);
        }
        return hash;
    }

    public static void deleteTempImages() {
        File root = PhotoHandle.getDir();
        FilenameFilter imgFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
            }
        };

        File[] imageFiles = root.listFiles(imgFilter);

        for (File image : imageFiles) {
            int label = Integer.parseInt(image.getName().split("\\-")[0]);
            if (label == 1) image.delete();
        }
    }

    public static void serviceProcess(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Check start service lock screen
        boolean isactive = prefs.getBoolean("checkboxPref", true);
        Intent intent = new Intent();
        intent.setClass(context, LockScreenService.class);
        if(isactive) {
            context.startService(intent);
            Toast.makeText(context, "Lock Screen ON", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(context, "Lock Screen OFF", Toast.LENGTH_SHORT).show();
            context.stopService(intent);
        }
    }

}
