package group21.lvtn.wowauthentication.utils;


import java.io.File;
import java.io.FilenameFilter;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Vector;
import group21.lvtn.wowauthentication.camera.PhotoHandle;

import org.bytedeco.javacpp.opencv_core.Mat;

import static group21.lvtn.wowauthentication.camera.PreProcessImage.ContrastImage;
import static group21.lvtn.wowauthentication.camera.PreProcessImage.reSize;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;

public class InputHandle {
    private Vector<Mat> src;
    private  Vector<String> labelMat;
    private Mat labels;
    private int number_of_classes;
    File[] imageFiles;

    public InputHandle() {
        
        File root = PhotoHandle.getDir();

        FilenameFilter imgFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
            }
        };

        imageFiles = root.listFiles(imgFilter);
        Arrays.sort(imageFiles);
        src = new Vector<>(imageFiles.length);
        labelMat = new Vector<>(imageFiles.length);
        labels = new Mat(imageFiles.length, 1, CV_32SC1);
        IntBuffer labelsBuf = labels.createBuffer();

        int counter = 0;
        number_of_classes = 0;
        HashSet<Integer> set_of_labels = new HashSet<>();

        for (File image : imageFiles) {
            Mat img = imread(image.getAbsolutePath());

            //Contrast image
             img = ContrastImage(img);

            //Run for performace
            //img = reSize(img, 0.3, 0.3);

            int label = Integer.parseInt(image.getName().split("\\-")[0]);

            set_of_labels.add(label);

           // Log.e("Images name: ", image.getName());
            src.add(img);
            labelMat.add(image.getName());
            labelsBuf.put(counter, label);

            counter++;
        }

        number_of_classes = set_of_labels.size();
    }

    public InputHandle(int perfomance) {

        File root = new File(PhotoHandle.getDir()+"/train");

        FilenameFilter imgFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
            }
        };

        imageFiles = root.listFiles(imgFilter);
        Arrays.sort(imageFiles);
        src = new Vector<>(imageFiles.length);
        labelMat = new Vector<>(imageFiles.length);
        labels = new Mat(imageFiles.length, 1, CV_32SC1);
        IntBuffer labelsBuf = labels.createBuffer();

        int counter = 0;
        number_of_classes = 0;
        HashSet<Integer> set_of_labels = new HashSet<>();

        for (File image : imageFiles) {
            Mat img = imread(image.getAbsolutePath());

            //Contrast image
            img = ContrastImage(img);

            //Run for performace
            img = reSize(img, 0.3, 0.3);

            int label = Integer.parseInt(image.getName().split("\\-")[0]);

            set_of_labels.add(label);

            // Log.e("Images name: ", image.getName());
            src.add(img);
            labelMat.add(image.getName());
            labelsBuf.put(counter, label);

            counter++;
        }

        number_of_classes = set_of_labels.size();
    }

    public int getNumber_of_classes() {
        return number_of_classes;
    }

    public Mat getLables() {
        return labels;
    }

    public Vector<String> getLabelMat(){ return labelMat;}

    public Vector<Mat> getSrc() {
        return src;
    }

}
