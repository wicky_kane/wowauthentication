package group21.lvtn.wowauthentication.utils;

import android.util.Log;
import org.bytedeco.javacpp.opencv_core.FileStorage;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import group21.lvtn.wowauthentication.feature_extraction.Eigenface;
import group21.lvtn.wowauthentication.feature_extraction.Fisherface;
import group21.lvtn.wowauthentication.feature_extraction.LBP;

/**
 * Created by wicky on 9/14/2016.
 */
public class StoreModel {

    public static int size;

    public static void saveEigenface(String file_name,Eigenface eigen)
    {
        FileStorage fs = new FileStorage(file_name,FileStorage.WRITE);
        opencv_core.write(fs,"eigenvector", eigen._eigenvectors);
        opencv_core.write(fs, "mean", eigen._mean);
        fs.release();
    }

    public static void saveProjectEigenface(String file_name, Vector<Mat> _projection)
    {
        FileStorage fs = new FileStorage(file_name,FileStorage.WRITE);
        for(int i = 0; i<_projection.size(); i++) {
            opencv_core.write(fs, "projection" + String.valueOf(i), _projection.get(i));
        }
        fs.release();
    }

    public static void saveFisherface(String file_name, Fisherface fisher)
    {
        FileStorage fs = new FileStorage(file_name, FileStorage.WRITE);
        opencv_core.write(fs,"eigenvector", fisher._eigenvectors);
        opencv_core.write(fs, "mean", fisher._mean);
        opencv_core.write(fs, "label", fisher._labels);
        fs.release();
    }

    public static void saveProjectFisherface(String file_name, Vector<Mat> _projection)
    {
        FileStorage fs = new FileStorage(file_name,FileStorage.WRITE);
        for(int i = 0; i<_projection.size(); i++) {
            opencv_core.write(fs, "projection" + String.valueOf(i), _projection.get(i));
        }
        fs.release();
    }

    public static void saveSketch(File file_name, Vector<Vector> sketch)
    {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
;
            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("vector");
            doc.appendChild(rootElement);

            for(int i = 0; i<sketch.size(); i++) {
                Element staff = doc.createElement("label");
                rootElement.appendChild(staff);

                staff.setAttribute("id", String.valueOf(i));

                staff.appendChild(doc.createTextNode(Vector2String(sketch.elementAt(i))));
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file_name);

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            Log.e("File saved!","Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public static void saveLBP(File file_name, LBP lbp)
    {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("trainingset");
            doc.appendChild(rootElement);
            //Log.e("LABEL",lbp.labels.elementAt(0).split("\\-")[0]);
            for(int i = 0; i<lbp.list_hist.size(); i++) {
                // staff elements
                //String label = lbp.labels.elementAt(0).split("\\-")[0];
                Element staff = doc.createElement("Label");
                rootElement.appendChild(staff);

                // set attribute to staff element
                //staff.setAttribute("id", lbp.labels.elementAt(i).split("\\-")[0]);
                staff.setAttribute("id", lbp.labels.elementAt(i));

                staff.appendChild(doc.createTextNode(GetAHistToString(lbp.list_hist.elementAt(i))));
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file_name);

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            Log.e("File saved!","Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    private static String GetAHistToString(int[] hist){
        String strHist ="";
        for (int i=0;i<hist.length;i++){
            strHist+=String.valueOf(hist[i]);
            if (i!=hist.length-1) strHist+=" ";
        }
        return strHist;
    }

    //FOR SKETCH
    private static String Vector2String(Vector hist){
        String strHist ="";
        for (int i=0;i<hist.size();i++){
            strHist+=String.valueOf(hist.get(i));
            if (i!=hist.size()-1) strHist+=" ";
        }
        return strHist;
    }

    public static Eigenface readEigenface(String file_name,String projectFile, int num_components, double threshold)
    {
        //Read eigenvector and mean vector
        FileStorage fs = new FileStorage(file_name,FileStorage.READ);
        Mat eigenvector = new Mat();
        Mat mean = new Mat();
        opencv_core.read(fs.get("eigenvector"), eigenvector);
        opencv_core.read(fs.get("mean"), mean);
        fs.release();

        //Read project vector
        Vector<Mat> projection = new Vector<Mat>();
        fs = new FileStorage(projectFile,FileStorage.READ);
        for(int i = 0; i<10; i++) {
            Mat mychild = new Mat();
            opencv_core.read(fs.get("projection" + String.valueOf(i)), mychild);
            projection.add(mychild);
        }
        fs.release();
        return  new Eigenface(num_components, threshold, eigenvector,mean, projection);
    }

    public static Eigenface readEigenface(String file_name, int num_components, double threshold)
    {
        //Read eigenvector and mean vector
        FileStorage fs = new FileStorage(file_name,FileStorage.READ);
        Mat eigenvector = new Mat();
        Mat mean = new Mat();
        opencv_core.read(fs.get("eigenvector"),eigenvector);
        opencv_core.read(fs.get("mean"), mean);
        fs.release();
        return  new Eigenface(num_components, threshold, eigenvector,mean, new Vector<Mat>());
    }

    public static Fisherface readFisherface(String file_name,String projectFile, int num_components, double threshold)
    {
        //Read eigenvector and mean vector, label
        FileStorage fs = new FileStorage(file_name,FileStorage.READ);
        Mat eigenvector = new Mat();
        Mat mean = new Mat();
        Mat label = new Mat();
        opencv_core.read(fs.get("eigenvector"),eigenvector);
        opencv_core.read(fs.get("mean"), mean);
        opencv_core.read(fs.get("label"), label);
        fs.release();

        //Read project vector
        Vector<Mat> projection = new Vector<Mat>();
        fs = new FileStorage(projectFile,FileStorage.READ);
        for(int i = 0; i<10; i++) {
            Mat mychild = new Mat();
            opencv_core.read(fs.get("projection" + String.valueOf(i)), mychild);
            projection.add(mychild);
        }
        fs.release();
        return  new Fisherface(num_components, threshold, eigenvector,mean, label, projection);
    }

    public static Fisherface readFisherface(String file_name, int num_components, double threshold)
    {
        FileStorage fs = new FileStorage(file_name,FileStorage.READ);
        Mat eigenvector = new Mat();
        Mat mean = new Mat();
        Mat label = new Mat();
        opencv_core.read(fs.get("eigenvector"),eigenvector);
        opencv_core.read(fs.get("mean"), mean);
        opencv_core.read(fs.get("label"), label);
        fs.release();
        return  new Fisherface(num_components, threshold, eigenvector, mean, label, new Vector<Mat>());
    }

    public static Vector<Vector> readSketch(String file_name, int size)
    {
        Vector<Vector> result=null;
        Vector<String> labels=null;
        try {

            File fXmlFile = new File(file_name);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("label");

            result = new Vector<>(nList.getLength());
            labels = new Vector<>(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    labels.add(eElement.getAttribute("id"));

                    String data = eElement.getTextContent();
                    Vector<Integer> ele = new Vector<Integer>(size);
                    for (int i=0; i<size; i++){
                        int value = Integer.parseInt(data.split(" ")[i]);
                        ele.add(value);
                    }

                    result.add(ele);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Vector<Vector> readHash(String file_name, int size)
    {
        Vector<Vector> result=null;
        Vector<String> labels=null;
        try {

            File fXmlFile = new File(file_name);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("label");

            result = new Vector<>(nList.getLength());
            labels = new Vector<>(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    labels.add(eElement.getAttribute("id"));

                    String data = eElement.getTextContent();
                    Vector<String> ele = new Vector<String>(size);
                    for (int i=0; i<size; i++){
                        ele.add(data.split(" ")[i]);
                    }
                    result.add(ele);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static LBP readLBP(String file_name)
    {
        Vector<int[]> list_hist=null;
        Vector<String> labels=null;
        try {
            File fXmlFile = new File(file_name);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            //Log.e("Root element :", doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("Label");

            //Log.e("readLBP","----------------------------");
            list_hist = new Vector<>(nList.getLength());
            labels = new Vector<>(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                //Log.e("Current Element :", nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    //Log.e("Label id1 : ", eElement.getAttribute("id")+"/"+String.valueOf(nList.getLength()));
                    labels.add(eElement.getAttribute("id"));
                    //Log.e("Label id2 : ", labels.elementAt(temp));
                    //Log.e("Content : ", eElement.getTextContent());
                    list_hist.add(GetAStringToHist(eElement.getTextContent()));
                    /*System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
                    System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
                    System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
                    System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());*/

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  new LBP(list_hist,labels,0);
    }

    private static int[] GetAStringToHist(String data){
        int[] hist= new int[256];
        for (int i=0;i<hist.length;i++){
            hist[i]= Integer.parseInt(data.split(" ")[i]);
        }
        return hist;
    }

    public static void writeTest(String file_name, Mat eigenvector, Mat mean){
        FileStorage fs = new FileStorage(file_name,FileStorage.WRITE);
        opencv_core.write(fs,"eigenvector", eigenvector);
        opencv_core.write(fs,"mean", mean);
        fs.release();

    }

}
