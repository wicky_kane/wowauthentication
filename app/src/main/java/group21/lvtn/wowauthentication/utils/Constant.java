package group21.lvtn.wowauthentication.utils;

/**
 * Created by wicky on 11/29/2016.
 */
public class Constant {
    public static final String XML_EIGENFACE = "eigenface.xml";
    public static final String XML_FISHERFACE = "fisherface.xml";
//    public static final String XML_LBP = "localbinarypattern.xml";
//    public static final String XML_PROJECT_LBP = "project_localbinarypattern.xml";
    public static final String XML_HASH_EIGENFACE = "hash_eigenface.xml";
    public static final String XML_HASH_FISHERFACE = "hash_fisherface.xml";
    public static final String XML_HASH_LBP = "hash_localbinarypattern.xml";
    public static final String XML_SKETCH_EIGENFACE = "sketch_eigenface.xml";
    public static final String XML_SKETCH_FISHERFACE = "sketch_fisherface.xml";
    public static final String XML_SKETCH_LBP = "sketch_localbinarypattern.xml";
    public static final  String SIGN_UP_INFO = "You must sign up first!";
    public static final String XML_EIGEN_MAIN = "eigenface_main.xml";
    public static final String XML_FISHER_MAIN = "fisherface_main.xml";
    public static final String XML_LBP_MAIN = "lbp_main.xml";
    public static final String EIGENGFACE = "0";
    public static final String FISHERFACE = "1";
    public static final String LBPH = "2";
    public static final String ALL = "3";
}
