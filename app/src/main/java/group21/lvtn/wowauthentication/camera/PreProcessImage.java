package group21.lvtn.wowauthentication.camera;

import android.util.Size;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacpp.opencv_highgui.*;
/**
 * Created by wicky on 10/19/2016.
 */
public class PreProcessImage {
    /// Global Variables

    static int MAX_KERNEL_LENGTH = 31;

    static public Mat BlurImage( Mat src )
    {
        Mat dst = src.clone();
        /// Applying Gaussian blur
        for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ) {
            opencv_core.Size size = new opencv_core.Size(i,i);
            opencv_imgproc.GaussianBlur(src, dst, size, 0.0, 0.0, opencv_core.BORDER_DEFAULT );
            //GaussianBlur( src, dst, Size( i, i ), 0, 0 );
        }
        return dst;
    }

    static public Mat ContrastImage(Mat src) {
        Mat dst = src.clone();
        /// Convert to grayscale
        try{opencv_imgproc.cvtColor(src, src, opencv_imgproc.CV_BGR2GRAY);}
        catch (Exception e){

        }
        /// Apply Histogram Equalization
        opencv_imgproc.equalizeHist(src, dst);
        return dst;
    }

    static public Mat Erosion( Mat src, int erosion_elem, int erosion_size )
    {
        Mat erosion_dst = src.clone();
        int erosion_type = 0;
        if( erosion_elem == 0 ){ erosion_type = opencv_imgproc.MORPH_RECT; }
        else if( erosion_elem == 1 ){ erosion_type = opencv_imgproc.MORPH_CROSS; }
        else if( erosion_elem == 2) { erosion_type = opencv_imgproc.MORPH_ELLIPSE; }

        Mat element = opencv_imgproc.getStructuringElement(erosion_type,
                new opencv_core.Size(2 * erosion_size + 1, 2 * erosion_size + 1),
                new opencv_core.Point(erosion_size, erosion_size));

        /// Apply the erosion operation
        opencv_imgproc.erode(src, erosion_dst, element);
       return erosion_dst;
    }

    static public Mat Dilation( Mat src, int dilation_elem, int dilation_size )
    {
        Mat dilation_dst = src.clone();
        int dilation_type = 0;
        if( dilation_elem == 0 ){ dilation_type = opencv_imgproc.MORPH_RECT; }
        else if( dilation_elem == 1 ){ dilation_type = opencv_imgproc.MORPH_CROSS; }
        else if( dilation_elem == 2) { dilation_type = opencv_imgproc.MORPH_ELLIPSE; }

        Mat element = opencv_imgproc.getStructuringElement(dilation_type,
                new opencv_core.Size(2 * dilation_size + 1, 2 * dilation_size + 1),
                new opencv_core.Point(dilation_size, dilation_size));
        /// Apply the dilation operation
        opencv_imgproc.dilate(src, dilation_dst, element);
        return dilation_dst;
    }

    static public Mat reSize( Mat src, double fx, double fy )
    {
        Mat dst = src.clone();
        opencv_imgproc.resize(dst, dst, new opencv_core.Size(), fx, fy, opencv_imgproc.INTER_LINEAR);
        return  dst;
    }

    static public Mat reSizeNormal( Mat src, double fx, double fy )
    {
        Mat dst = src.clone();
        opencv_imgproc.resize(dst, dst, new opencv_core.Size(), fx, fy, opencv_imgproc.INTER_LINEAR );
        return  dst;
    }
}
