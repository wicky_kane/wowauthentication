package group21.lvtn.wowauthentication.camera;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import org.bytedeco.javacpp.opencv_core;

import group21.lvtn.wowauthentication.authentication.AuthenticationActivity;

public class PhotoAuthenticationHandle implements CameraSource.PictureCallback {

    private final Context context;
    private Face myFace;
    private String imageName;
    public PhotoAuthenticationHandle(Context context, String imageName) {
        this.context = context;
        this.imageName = imageName;
    }

    @Override
    public void onPictureTaken(byte[] data) {
        Bitmap photo = BitmapFactory.decodeByteArray(data, 0, data.length);
        int width = photo.getWidth();
        int height = photo.getHeight();

        FaceDetector detector = new FaceDetector.Builder(context)
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .build();

        Frame frame = new Frame.Builder().setBitmap(photo).build();
        SparseArray<Face> faces = detector.detect(frame);
        for (int i = 0; i < faces.size(); ++i) {
            Face face = faces.valueAt(i);
            myFace = face;
        }
        if(myFace == null) {
            return;
        }
        detector.release();

        float[] myFaceSize = {myFace.getPosition().x, myFace.getPosition().y, myFace.getWidth(), myFace.getHeight()};
        int [] size = process(myFaceSize, width, height);

        Bitmap bitmap = cropImage(photo, size[0], size[1], size[2], size[3]);

        if (bitmap != null) {
            bitmap = ARGBBitmap(bitmap);
            saveImage(bitmap, imageName);
//            Toast.makeText(context,"Detected Face. Authenticating...",Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap ARGBBitmap(Bitmap img) {
        return img.copy(Bitmap.Config.ARGB_8888,true);
    }

    private Bitmap cropImage (Bitmap image, int x, int y, int width, int height) {

        int offset = Math.abs(width - height)/2;

        if (width > height) {
            y -= offset;
            height = width;
        }
        else {
            width = height;
            x -= offset;
        }

        Bitmap croppedBitmap;
        try {
            croppedBitmap = Bitmap.createBitmap(image, x, y, width, height);
        }
        catch (Exception error){
            return null;
        }

        Bitmap scale = bitmapResize(croppedBitmap, 150, 150);

        return scale;
    }

    private int[] process(float[] arr, int width, int height) {
        float scale = 1.0f;
        // float scale = height / (float)width;
        int[] size = new int[4];
        for (int i=0; i<4; i++) {
            size[i] = (int) (arr[i] *scale);
        }
        return size;
    }

    public Bitmap bitmapResize(Bitmap bitmap,int newWidth,int newHeight) {

        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }

    private void saveImage(Bitmap photo, String fileName){

        File pictureFileDir = PhotoHandle.getDir();

        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
            Toast.makeText(context, "Can't create directory to save image.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        String filename = pictureFileDir.getPath() + File.separator + fileName;
        File pictureFile = new File(filename);

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);

            photo.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();

        } catch (Exception error) {
            Toast.makeText(context, "Image could not be saved.", Toast.LENGTH_LONG).show();
        }
    }

}