package group21.lvtn.wowauthentication.feature_extraction;

import android.util.Log;

import java.util.Vector;


import org.bytedeco.javacpp.opencv_core;
import static org.bytedeco.javacpp.opencv_core.PCA;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import org.bytedeco.javacpp.opencv_core.Mat;

import group21.lvtn.wowauthentication.utils.InputHandle;

public class Eigenface extends opencv_core.Algorithm {

	private static int _num_components;
	public static double _threshold;
	public static Vector<Mat> _projection;
	public static Mat _labels;
	public static Mat _eigenvectors;
	public static Mat _mean;

	public Eigenface(){
		_num_components = 0;
		_threshold = Float.MAX_VALUE;
	};

	public Eigenface(int num_components, double threshold){
		this._num_components = num_components;
		this._threshold = threshold;
	};

	public Eigenface(int num_components, double threshold, Mat eigenvector, Mat mean, Vector<Mat> projection){
		this._num_components = num_components;
		this._threshold = threshold;
		this._eigenvectors = eigenvector;
		this._mean = mean;
		this._projection = projection;
	};

	public Eigenface(Vector<Mat> src, Mat labels, int num_compenents, double threshold){
		this._num_components = num_compenents;
		this._threshold = threshold;
		compute(src, labels);
	};
	
	public static Mat asRowMatrix(Vector<Mat> src, int rtype){
		double alpha = 1.0;
		double beta = 0.0;

		int n = src.size();
		if(n == 0)
			return new Mat();
		int d = (int) src.get(0).total();

		Mat data = new Mat(n, d, rtype);
		for(int i = 0;i < n; i++){
			if((int)src.get(i).total() != d){
				throw new RuntimeException("Wrong size d");
			}
			Mat xi = data.row(i);
			if(src.get(i).isContinuous()){
				src.get(i).reshape(1,1).convertTo(xi, rtype, alpha, beta);
			} else {
				src.get(i).clone().reshape(1,1).convertTo(xi, rtype, alpha, beta);
			}
		}
			return data;
	}

	public Mat getAuthenMat(Mat src) {

		Log.e("TEST","total: "+ src.total()+ " hang: "+ src.rows()+ " cot: "+ src.cols());
		if(this._eigenvectors.cols() != src.total()) {
			// check data alignment just for clearer exception messages
			throw new RuntimeException("Wrong input image size. Reason: Training and Test images must be of equal size!");
		}

		Mat q = new Mat();
		opencv_core.PCAProject(src.reshape(1, 1).clone(), _mean, _eigenvectors, q);
		return q;
	}

	private void compute(Vector<Mat> src, Mat labels){
		if(src.size() == 0 ){
			throw new RuntimeException("Empty training data was given.");
		}
		Mat data = asRowMatrix(src, opencv_core.CV_64FC1);
		int n = data.rows();
		int d = data.cols();

		if(n != labels.rows()){
			throw new RuntimeException("The number of samples (src) must equal the number of labels (labels).");
		}
		
		if(_num_components <= 0 || _num_components > n){
			_num_components = n;
		}

		Mat eigenvector = new Mat();
		Mat mean = new Mat();
		opencv_core.PCACompute(data, mean, eigenvector, _num_components);

		_mean = mean.reshape(1, 1);
		_eigenvectors = eigenvector;
		_labels = labels;

	}

	public void doProject(Vector<Mat> src){
		_projection = new Vector<Mat>();
		for(int sampleIdx = 0; sampleIdx < src.size(); sampleIdx++) {
			Mat p = new Mat();
			opencv_core.PCAProject(src.get(sampleIdx).reshape(1,1).clone(), _mean, _eigenvectors,p);
			_projection.add(p);
		}
	}

	private  Mat project(Mat src, Mat _eigenvectors, Mat _mean){
		Mat W = _eigenvectors;
		Mat mean = _mean;
		int n = src.rows();
		int d = src.cols();
		if(W.rows() != d) {
			throw new RuntimeException("Wrong shapes for given matrices");
		}

		if(!mean.empty() && mean.total() != d) {
			throw new RuntimeException("Wrong mean shape for the given data matrix. Mean: "+ String.valueOf(mean.total())+" d: "+ String.valueOf(d));
		}
		Mat X = new Mat();
		Mat Y = new Mat();
		src.convertTo(X, W.type());
		if(!mean.empty()){
			for(int i = 0; i<n; i++){
				Mat r_i = X.row(i);
				opencv_core.subtract(r_i, mean.reshape(1, 1), r_i);
			}
		}
		opencv_core.gemm(X, W, 1.0, new Mat(), 0.0, Y);
		return Y;
	}

	public int predictEigenface(Mat src) {
		int minClass;
		double minDist;

		if(this._projection.isEmpty()) {
			// throw error if no data (or simply return -1?)
			throw new RuntimeException("Eigenfaces model is not computed yet. Did you call cv::Eigenfaces::train?");

		} else if(this._eigenvectors.cols() != src.total()) {
			// check data alignment just for clearer exception messages
			throw new RuntimeException("Wrong input image size. Reason: Training and Test images must be of equal size!");
		}

		Mat q = new Mat();
		opencv_core.PCAProject(src.reshape(1,1).clone(),_mean, _eigenvectors, q);
		// find 1-nearest neighbor
		minDist = Float.MAX_VALUE;
		minClass = -1;

		for(int sampleIdx = 0; sampleIdx < _projection.size(); sampleIdx++) {
			double dist = opencv_core.norm(_projection.get(sampleIdx), q, opencv_core.NORM_L2, new Mat());
			Log.e("Distance", "Distance Eigen Threshold "+ String.valueOf(_threshold)+" : "  + String.valueOf(dist));
			if((dist < minDist) && (dist < _threshold)) {
				minDist = dist;
				minClass = 1;
				//break;
			}
		}
		return minClass;
	}

	public static Eigenface trainEigenface(int num_components, float threshold){
		InputHandle input = new InputHandle();
		Eigenface eigen = new Eigenface(input.getSrc(), input.getLables(), num_components, threshold);
		return eigen;
	}

}
