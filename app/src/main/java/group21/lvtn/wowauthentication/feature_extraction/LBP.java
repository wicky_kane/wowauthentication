package group21.lvtn.wowauthentication.feature_extraction;

import android.util.Log;

import org.bytedeco.javacpp.indexer.UByteBufferIndexer;
import org.bytedeco.javacpp.opencv_core.Mat;

import java.util.Vector;

import group21.lvtn.wowauthentication.utils.InputHandle;

/**
 * Created by Administrator on 09/10/2016.
 */
public class LBP {
    int p = 8, r = 1;
    public static Vector<int[][]> list_lbp;
    public static Vector<int[]> list_hist;
    public static Vector<String> labels;
    public static int FROM_TRAIN = 1, FROM_AUTHEN = 2;

    //For read from SD and caculate (no secure)
    public LBP(Vector<int[]> list_hist, Vector<String> labels, int forread){
        //Log.e("test Read to lbp:",labels.elementAt(0)+"="+String.valueOf(list_hist.elementAt(0)[0]));
        this.list_hist=list_hist;
        this.labels=labels;
        //Log.e("test Read to lbp:",this.labels.elementAt(0)+"="+String.valueOf(this.list_hist.elementAt(0)[0]));
    }

    //For total begin run, create 90 person train (call at PerformaceActivity)
    public LBP(Vector<Mat> src, Vector<String> labels){
        //init content list
        list_lbp = new Vector<>(src.size());
        list_hist = new Vector<>(src.size());
        this.labels = labels;

        //compute
        computeLBP(src, labels);
//        computeHist(list_lbp, labels);

        //Log.e("Num of LBP",String.valueOf(list_lbp.size()));
        //Log.e("Num of Hist",String.valueOf(list_hist.size()));
    };

    //For performance
    public LBP(Vector<Mat> src){
        //init content list
        list_lbp = new Vector<>(src.size());
        list_hist = new Vector<>(src.size());

        //compute
        computeLBP(src, labels);
//        computeHist(list_lbp, labels);

        //Log.e("Num of LBP",String.valueOf(list_lbp.size()));
        //Log.e("Num of Hist",String.valueOf(list_hist.size()));
    };

    public static int[] getAuthenHist(Mat src){
        return createHist(createLBP(src,FROM_AUTHEN));
    }

    private void computeLBP(Vector<Mat> src, Vector<String> labels){
        //Log.e("Number of Img",String.valueOf(src.size()));
        //Log.e("Number of Label",String.valueOf(labels.size()));
        for (int i=0;i<src.size();i++){
        //for (int i=0;i<10;i++){//Train anh label 1
        //for (int i=10;i<20;i++){
            //Log.e("Size Img",String.valueOf(src.get(i).rows())+"x"+String.valueOf(src.get(i).cols()));
            //Log.e("Label Img",String.valueOf(labels.elementAt(i)));
            list_lbp.addElement(createLBP(src.get(i),FROM_TRAIN));
        }
    }

    private static int[][] createLBP(Mat src, int from){
        int[][] dst = new int[src.rows()][src.cols()];
        int[] hist= new int[256];
        String code ="";
        UByteBufferIndexer sI = src.createIndexer();
        int count=0;
        for (int i = 1; i < src.rows()-1; i++) {
            for (int j = 1; j < src.cols()-1; j++) {
                int center = sI.get(i, j);
                code += (sI.get(i-1,j-1) > center) ? "1": "0";
                code += (sI.get(i-1,j) > center) ? "1": "0";
                code += (sI.get(i-1,j+1) > center) ? "1": "0";
                code += (sI.get(i,j+1) > center) ? "1": "0";
                code += (sI.get(i+1,j+1) > center) ? "1": "0";
                code += (sI.get(i+1,j) > center) ? "1": "0";
                code += (sI.get(i+1,j-1) > center) ? "1": "0";
                code += (sI.get(i,j-1) > center) ? "1": "0";
                //Log.e("Pixel gray value",String.valueOf(sI.get(i, j)));
                //Log.e("Pixel lbp value",String.valueOf(code)+"="+String.valueOf(Integer.parseInt(code, 2)));
                dst[i][j]= Integer.parseInt(code, 2);
                if (from==FROM_TRAIN) hist[dst[i][j]]++;
                code="";
                count++;
            }
        }
        if (from==FROM_TRAIN) list_hist.addElement(hist);
//        Log.e("Kich co 1 anh",String.valueOf(src.rows())+" x "+String.valueOf(src.cols()));
//        Log.e("Tong so pixel duoc dem",String.valueOf(count));
        return dst;
    }

    private void computeHist(Vector<int[][]> list_lbp, Vector<String> labels){
        for (int i=0;i<list_lbp.size();i++){
            list_hist.addElement(createHist(list_lbp.get(i)));
        }
    }

    private static int[] createHist(int[][] src){
        int[] hist= new int[256];
        for (int i = 0; i < src.length; i++) {
            for (int j = 0; j < src[0].length; j++) {
                hist[src[i][j]]++;
            }
        }
        return hist;
    }

    //Predict for normal use
    public int predict(Mat src, double threshold){
        int[][] inputLBP = createLBP(src,FROM_AUTHEN);
        int[] inputHist = createHist(inputLBP);
        double result = 0.0;
        double tmp=0;
        Log.e("Size hist in predict",String.valueOf(list_hist.size()));
        for (int i=0;i<this.list_hist.size();i++){
            tmp = chi_square(inputHist, list_hist.elementAt(i));
            result+=tmp;
            Log.e("Chi-squari result", String.valueOf(tmp) + "==========");
        }
        Log.e("Chi-squari FINAL result", String.valueOf(result/this.list_hist.size()) + " ==========");
        if (result/this.list_hist.size()<threshold) return 1;
        else return -1;
    }

    //Predict for performance
    public int predictPerformance(Mat src, double threshold){
        int[][] inputLBP = createLBP(src,FROM_AUTHEN);
        int[] inputHist = createHist(inputLBP);
        double result = 0.0;
        double tmp=0;
        //Log.e("Size hist in predict",String.valueOf(list_hist.size()));
        for (int i=0;i<this.list_hist.size();i++){
            tmp = chi_square(inputHist, list_hist.elementAt(i));
            result+=tmp;
            //Log.e("Chi-squari result", String.valueOf(tmp) + "==========");
        }
        //Log.e("Chi-squari FINAL result", String.valueOf(result/this.list_hist.size()) + " ==========");
        if (result/this.list_hist.size()<threshold) return 1;
        else return 0;
    }

    private double chi_square(int[] histogram0, int[] histogram1){
        if(histogram0.length != histogram1.length)
            Log.e("Histogram error", "Histograms must be of equal dimension."+
                    String.valueOf(histogram0.length)+"-"+String.valueOf(histogram1.length));
        double result = 0.0;
        for(int i=0; i < histogram0.length; i++) {
            double a = histogram0[i] - histogram1[i];
            double b = histogram0[i] + histogram1[i];
            if(Math.abs(b) > 0) {
                result+=(a*a)/b;
            }
        }
        return result;
    }

    /*
    * Create 90 person training set to project
    */
    public static LBP trainLBP(){
        InputHandle input = new InputHandle();
        LBP lbp = new LBP(input.getSrc(), input.getLabelMat());
        return lbp;
    }

    /*
    * Register for main person to authentication
    */
    public static LBP registerLBP(){
        LBP lbp = null;
        return lbp;
    }
}
