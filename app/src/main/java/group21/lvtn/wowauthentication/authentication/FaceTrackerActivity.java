/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package group21.lvtn.wowauthentication.authentication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;


import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.IntBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Vector;

import cn.pedant.SweetAlert.SweetAlertDialog;
import group21.lvtn.wowauthentication.UI.MainActivity;
import group21.lvtn.wowauthentication.camera.FaceGraphic;
import group21.lvtn.wowauthentication.camera.PhotoHandle;
import group21.lvtn.wowauthentication.lockscreenservice.LockScreenService;
import group21.lvtn.wowauthentication.settings.PrefManager;
import group21.lvtn.wowauthentication.R;
import group21.lvtn.wowauthentication.feature_extraction.Eigenface;
import group21.lvtn.wowauthentication.feature_extraction.Fisherface;
import group21.lvtn.wowauthentication.feature_extraction.LBP;
import group21.lvtn.wowauthentication.camera.CameraSourcePreview;
import group21.lvtn.wowauthentication.camera.GraphicOverlay;
import group21.lvtn.wowauthentication.security.Codebook;
import group21.lvtn.wowauthentication.security.HmacSha1Signature;
import group21.lvtn.wowauthentication.utils.Constant;
import group21.lvtn.wowauthentication.utils.HelpMethod;
import group21.lvtn.wowauthentication.utils.InputHandle;
import group21.lvtn.wowauthentication.utils.StoreModel;

import org.bytedeco.javacpp.indexer.IntBufferIndexer;
import org.bytedeco.javacpp.opencv_core.Mat;

import static org.bytedeco.javacpp.opencv_core.CV_32SC1;

@SuppressLint("NewApi")
public final class FaceTrackerActivity extends AppCompatActivity {
    private static final String TAG = "FaceTracker";
    public static final String HMAC_KEY = "#thisiswhatyoucamefor";

    private CameraSource mCameraSource = null;
    private SweetAlertDialog pDialog;
    private CameraSourcePreview mPreview;
    private TextView numberPhoto;
    private GraphicOverlay mGraphicOverlay;
    private Button mSnapBtn;
    private Button mDoneBtn;
    private int indexPhoto = 1;

    private final int DELTA_PCA = 100;
//    private final int DELTA_FLD = 30;15//0 1 0 0 0 1 2 0 1
    private final int DELTA_FLD =25;//
    private final int DELTA_LBP = 100;//22x 236 237
    //Setting manager
    PrefManager prefManager;
    SharedPreferences pref;

    private Face myFace; //store position of the face.

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    //==============================================================================================
    // Activity Methods
    //==============================================================================================
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * Initializes the UI and initiates the creation of a face detector.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.main);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);
        mSnapBtn = (Button) findViewById(R.id.snap_btn);
        mDoneBtn = (Button) findViewById(R.id.done_btn);
        numberPhoto = (TextView) findViewById(R.id.numberPhoto);
        prefManager = new PrefManager(this);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }

        pref = PreferenceManager.getDefaultSharedPreferences(getApplication());

        mSnapBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (indexPhoto > 10) return;
                showLoading("Processing...");
                final PhotoHandle myHandle = new PhotoHandle(0, (indexPhoto % 11), pDialog, FaceTrackerActivity.this,
                        myFace, mCameraSource.getPreviewSize().getWidth(), mCameraSource.getPreviewSize().getHeight());
                mCameraSource.takePicture(null, myHandle);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (myHandle.isHandleSuccess()) {
                            numberPhoto.setText(String.valueOf((indexPhoto % 11) + "/10"));
                            if (indexPhoto % 10 == 0) mDoneBtn.setVisibility(View.VISIBLE);
                            indexPhoto++;
                        }
                    }
                }, 1000);


            }
        });

        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Run training
                doSignUp();
            }
        });

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void doSignUp(){
        InputHandle input = new InputHandle();
        //Training eigen
        EigenfaceSignUp eigenTraining = new EigenfaceSignUp();
        eigenTraining.execute(input);

        //Training fisher
        FisherfaceTraining fisherTraining = new FisherfaceTraining();
        fisherTraining.execute(input);

        //Training LBP
        LBPTraining lbpTraining = new LBPTraining();
        lbpTraining.execute(input);
    }

    @Override
    public void onStart() {
        super.onStart();
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "FaceTracker Page", // TODO: Define a title for the content shown.
                Uri.parse("http://host/path"),
                Uri.parse("android-app://group21.lvtn.wowauthentication/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "FaceTracker Page", // TODO: Define a title for the content shown.
                Uri.parse("http://host/path"),
                Uri.parse("android-app://group21.lvtn.wowauthentication/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /**
     * Training Async Tasks - Eigenface.
     */

    private void saveTrainingEigen(){
        Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_eigenface), getString(R.string.pref_threshold_default)));
        Eigenface eigen = Eigenface.trainEigenface(30, threshold);
        String pathSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_EIGENFACE;
        StoreModel.saveEigenface(pathSave, eigen);

    }

    private class EigenfaceSignUp extends AsyncTask<InputHandle, Void, String> {
        @Override
        protected void onPreExecute() {
            showLoading("Training...");
        }

        @Override
        protected String doInBackground(InputHandle... input) {

//            InputHandle input = new InputHandle();
            Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_eigenface), getString(R.string.pref_threshold_default)));
            String pathSave = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_EIGENFACE;
            Eigenface eigen = StoreModel.readEigenface(pathSave, 30, threshold);
            eigen.doProject(input[0].getSrc());

            //Show vector dac trung
//            for(int i = 0; i< eigen._projection.size(); i++){
//                Log.e("TEST","vector ori eigen"+ i +": "+ Codebook.Mat2Vector(eigen._projection.get(i)));
//            }

            if(prefManager.isSecurity()) {
                //Hash and gen sketch
                String pathHash = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_HASH_EIGENFACE;
                File saveHash = new File(pathHash);
                if (saveHash.exists()) saveHash.delete();

                //Sinh va luu sketch
                float DELTA = DELTA_PCA;
                Vector<Vector> sketch = Codebook.codebookGenerator(eigen._projection, (int) DELTA);
                String pathSketch = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_SKETCH_EIGENFACE;
                File saveSketch = new File(pathSketch);
                if (saveSketch.exists()) saveSketch.delete();
                StoreModel.saveSketch(saveSketch, sketch);

                //Show vector sketch eigenface
//            for(int i = 0; i< sketch.size(); i++){
//                Log.e("TEST","sketch ori eigen"+ i +": "+ sketch.get(i));
//            }

                try {
                    Vector<Vector> hash = HelpMethod.hashTemplateOri(eigen._projection);
                    StoreModel.saveSketch(saveHash, hash);

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (SignatureException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
            else {
                String pathHash = PhotoHandle.getSaveDir().getPath() + File.separator + Constant.XML_EIGEN_MAIN;
                StoreModel.saveProjectEigenface(pathHash, eigen._projection);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //pDialog.cancel();
//            Toast.makeText(getApplication(), "EIGEN Training successfull!", Toast.LENGTH_LONG).show();
            prefManager.setSignedUp(false);

            //Log.e("EIGEN ", "TRAINING SUCCESS");
            //finish();
        }
    }

    private  void saveTrainingFisherface(){
        Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_fisherface),
                getString(R.string.pref_threshold_default)));
        Fisherface fisher = Fisherface.trainFisherface(90, threshold);
        String pathSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_FISHERFACE;
        File saveFile = new File(pathSave);
        StoreModel.saveFisherface(pathSave, fisher);
    }

    private class FisherfaceTraining extends AsyncTask<InputHandle, Void, String> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(InputHandle... input) {

//            InputHandle input = new InputHandle();

            Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_fisherface),
                    getString(R.string.pref_threshold_default)));
            String pathSave = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_FISHERFACE;
            Fisherface fisher = StoreModel.readFisherface(pathSave, 90, threshold);
            fisher.doProject(input[0].getSrc());

            if(prefManager.isSecurity()) {
                String pathHash = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_HASH_FISHERFACE;
                File saveHash = new File(pathHash);
                if (saveHash.exists()) saveHash.delete();
                //Sinh va luu sketch
//            float DELTA = 42;
                float DELTA = DELTA_FLD;
                Vector<Vector> sketch = Codebook.codebookGenerator(fisher._projection, (int) DELTA);
                String pathSketch = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_SKETCH_FISHERFACE;
                File saveSketch = new File(pathSketch);
                if (saveSketch.exists()) saveSketch.delete();
                StoreModel.saveSketch(saveSketch, sketch);

                try {
                    Vector<Vector> hash = HelpMethod.hashTemplateOri(fisher._projection);
                    StoreModel.saveSketch(saveHash, hash);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (SignatureException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
            else {
                String pathHash = PhotoHandle.getSaveDir().getPath() + File.separator + Constant.XML_FISHER_MAIN;
                StoreModel.saveProjectEigenface(pathHash, fisher._projection);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            Toast.makeText(getApplication(), "FISHER Training successfull!", Toast.LENGTH_LONG).show();
            prefManager.setSignedUp(false);
            //Log.e("FISHER ", "TRAINING SUCCESS");
           // finish();
        }
    }

    private class LBPTraining extends AsyncTask<InputHandle, Void, String> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(InputHandle... input) {
            //Dang ki cho 10 tam moi chup
//            InputHandle input = new InputHandle();
            LBP lbp = new LBP(input[0].getSrc(), input[0].getLabelMat());

            /*String hist150="";
            for (int k=0;k<lbp.list_hist.size();k++){
                hist150="";
                for (int i=0;i<256;i++){
                    hist150+= lbp.list_hist.get(k)[i]+"_";
                }
                Log.e("hist150",hist150);
            }*/

            if(prefManager.isSecurity()) {
                Vector<Mat> list_mat_hist = new Vector<>(lbp.list_hist.size());
                for (int n=0;n<lbp.list_hist.size();n++){
                    Mat hist =  new Mat(256, 1, CV_32SC1);
                    IntBuffer histBuf = hist.createBuffer();
                    for (int i=0;i<256;i++){
                        histBuf.put(i, lbp.list_hist.get(n)[i]);
                    }
                    list_mat_hist.add(hist);
                }

                /*for (int k=0;k<list_mat_hist.size();k++){
                    hist150="";
                    IntBufferIndexer idx = list_mat_hist.elementAt(k).createIndexer();
                    for (int i=0;i<256;i++){
                        hist150+= idx.get(0, i)+"_";
                    }
                    Log.e("Mat150",hist150);
                }*/

                //Hash and gen sketch
                String pathHash = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_HASH_LBP;
                File saveHash = new File(pathHash);
                if(saveHash.exists()) saveHash.delete();

                //Sinh va luu sketch
                String pathSketch = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_SKETCH_LBP;
                File saveSketch = new File(pathSketch);
                if(saveSketch.exists()) saveSketch.delete();

                Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_lbp),
                        getString(R.string.pref_threshold_default)));

                //Sinh va luu Codebook
                float DELTA = DELTA_LBP;
                Vector<Vector> sketch = Codebook.codebookGenerator(list_mat_hist, (int) DELTA);
                StoreModel.saveSketch(saveSketch, sketch);

                try {
                    Vector<Vector> hash = HelpMethod.hashTemplateOri(list_mat_hist);
                    StoreModel.saveSketch(saveHash, hash);

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (SignatureException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
            else {
                String pathHash = PhotoHandle.getSaveDir().getPath() + File.separator + Constant.XML_LBP_MAIN;
                File lbpfile = new File(pathHash);
                StoreModel.saveLBP(lbpfile,lbp);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            Toast.makeText(getApplication(), "LBP Training successfull!", Toast.LENGTH_LONG).show();
//            pDialog.cancel();
            pDialog.dismiss();
            prefManager.setSignedUp(false);

            //Delete image
            HelpMethod.deleteTempImages() ;
            Context context = getApplication();
            HelpMethod.serviceProcess(context);
//            finishAffinity();
//            finishAndRemoveTask();
            System.exit(0);
        }
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        //Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {

        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            //Log.w(TAG, "Face detector dependencies are not yet available.");
        }
        int cameraFacing = CameraSource.CAMERA_FACING_BACK;
        if (Camera.getNumberOfCameras() > 1) {
            cameraFacing = CameraSource.CAMERA_FACING_FRONT;
        }

        int Measuredwidth = 0;
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }

        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(Measuredheight, Measuredwidth)
                //.setRequestedPreviewSize(2134, 1200)
                // .setRequestedPreviewSize(Resources.getSystem().getDisplayMetrics().widthPixels,Resources.getSystem().getDisplayMetrics().heightPixels)
                .setFacing(cameraFacing)
                .setRequestedFps(30.0f)
                .build();

    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        //OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
        super.onResume();
        startCameraSource();
    }

    /**
     * Ohters functions.
     */

    private void showLoading(String text) {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(text);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
    }

    private void showDialogWithConfirm() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to cancel this process.")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        HelpMethod.deleteTempImages();
                        prefManager.setSignedUp(false);
                        Intent intent = new Intent(getApplication(), MainActivity.class);
                        startActivity(intent);
                        sweetAlertDialog.dismiss();
                        finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * Press back button.
     */
    @Override
    public void onBackPressed() {
        showDialogWithConfirm();
        //super.onBackPressed();
    }

    /**
     * Stops the camera.
     */

    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            //Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        //Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
          //      " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Face Tracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                //Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
            myFace = face;
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }



}
