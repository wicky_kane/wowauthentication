package group21.lvtn.wowauthentication.authentication;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import org.bytedeco.javacpp.opencv_core.Mat;

import java.io.File;
import java.io.IOException;

import java.nio.IntBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Vector;

import cn.pedant.SweetAlert.SweetAlertDialog;
import group21.lvtn.wowauthentication.UI.MainActivity;
import group21.lvtn.wowauthentication.camera.FaceGraphic;
import group21.lvtn.wowauthentication.camera.PhotoAuthenticationHandle;
import group21.lvtn.wowauthentication.camera.PhotoHandle;
import group21.lvtn.wowauthentication.R;
import group21.lvtn.wowauthentication.feature_extraction.Eigenface;
import group21.lvtn.wowauthentication.feature_extraction.Fisherface;
import group21.lvtn.wowauthentication.feature_extraction.LBP;
import group21.lvtn.wowauthentication.camera.CameraSourcePreview;
import group21.lvtn.wowauthentication.camera.GraphicOverlay;
import group21.lvtn.wowauthentication.lockscreenservice.LockScreenService;
import group21.lvtn.wowauthentication.screenoffservice.ReceiverClass;
import group21.lvtn.wowauthentication.security.Codebook;
import group21.lvtn.wowauthentication.settings.PrefManager;
import group21.lvtn.wowauthentication.utils.Constant;
import group21.lvtn.wowauthentication.utils.HelpMethod;
import group21.lvtn.wowauthentication.utils.StoreModel;

import static group21.lvtn.wowauthentication.camera.PreProcessImage.ContrastImage;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

/**
 * Activity for the face tracker app.  This app detects faces with the rear facing camera, and draws
 * overlay graphics to indicate the position, size, and ID of each face.
 */
public final class AuthenticationActivity extends AppCompatActivity {
    private static final String TAG = "FaceTracker";

    private CameraSource mCameraSource = null;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    private SharedPreferences pref;
    PrefManager prefManager;
    private String algorithm; /* 0-eigenface, 1-fisherface, 2-lbp, 3-all;*/
    private Face myFace; //store position of the face.
    private TextView status_text;
    private int count = 0; //Number to turn of

    private int PCA_THRESHOLD = 2500;
    private int FLD_THRESHOLD = 1000;
    private int LBP_THRESHOLD = 430;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    public static boolean isLocked = false;

    //==============================================================================================
    // For screen off
    protected static final int REQUEST_ENABLE = 0;
    private DevicePolicyManager devicePolicyManager;
    private ComponentName adminComponent;

    //=====================
    //

    //==============================================================================================
    // Activity Methods
    //==============================================================================================
    /**
     * Initializes the UI and initiates the creation of a face detector.
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        setContentView(R.layout.activity_authen);

        prefManager = new PrefManager(this);
        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        algorithm = pref.getString(getString(R.string.pref_algorithm_key), getString(R.string.pref_algorithm_default));
        status_text = (TextView) findViewById(R.id.status_text);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }

        try {
            startService(new Intent(this, LockScreenService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         *  Auto dectect and authenticate every 3s
         */
        runAutoDetect();

    }

    private  void runAutoDetect(){
        final Handler thandler = new Handler();
        final long timedelay = 6000;
        final Runnable timerRun= new Runnable()
        {
            @Override
            public void run() {
                status_text.setText("Authenticating ...");
                if(myFace != null) {
                    myFace = null;
                    boolean test = doAuthentication();

                    if (test) {
                        Intent resultIntent = new Intent();
                        setResult(Activity.RESULT_OK, resultIntent);
                        virbate();
                        thandler.removeCallbacks(this);
                        finish();
                        return;
                    }
                    else {
                        count++;
//                        Toast.makeText(getApplication(), "Access denied "+count+" times. Continue..." , Toast.LENGTH_SHORT).show();
                        status_text.setText("Access denied "+count+" times. Continue...");
                    }
                }

                //Qua 5 lan thi se tat va khoa man hinh
                if(count == 5){
                    thandler.removeCallbacks(this);
                    showDialogWithConfirm();
                    count=0;
                    status_text.setText("");
                    return;
                }
                thandler.postDelayed(this, timedelay);
            }
        };
        thandler.postDelayed(timerRun, timedelay);
    }

    private boolean doAuthentication() {
        boolean test= false;
        switch (algorithm){
            case Constant.EIGENGFACE:
                test = doAuthenticationEigen(false);
                break;
            case Constant.FISHERFACE:
                test = doAuthenticationFisher(false);
                break;
            case Constant.LBPH:
                test = doAuthenticationLBP(false);
                break;
            case Constant.ALL:
                boolean result_eigen = doAuthenticationEigen(true);
                boolean result_fisherface = doAuthenticationFisher(true);
                boolean result_lbp = doAuthenticationLBP(true);
                test = (result_eigen && result_fisherface)
                        || (result_eigen && result_lbp)
                        || (result_fisherface && result_lbp);
                break;
            default: break;
        }
        return test;
    }

    private void requestCameraPermission() {
        //Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {

        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            //Log.w(TAG, "Face detector dependencies are not yet available.");
        }
        int cameraFacing = CameraSource.CAMERA_FACING_BACK;
        if(Camera.getNumberOfCameras()>1) {
            cameraFacing = CameraSource.CAMERA_FACING_FRONT;
        }

        int Measuredwidth = 0;
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        }else{
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }

        //Log.e("Err", "width: height: " + String.valueOf(Measuredwidth) + ":" + String.valueOf(Measuredheight));
        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(Measuredheight, Measuredwidth)
                        //.setRequestedPreviewSize(2134, 1200)
                        // .setRequestedPreviewSize(Resources.getSystem().getDisplayMetrics().widthPixels,Resources.getSystem().getDisplayMetrics().heightPixels)
                .setFacing(cameraFacing)
                .setRequestedFps(30.0f)
                .build();

    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_HOME)) {
            // Key code constant: Home key. This key is handled by the framework and is never delivered to applications.
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        virbate();
        finish();
        return;
    }


    /**
     * virbate means that the screen is unlocked success
     */

    private void virbate() {
        Vibrator vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(200);
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            //Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        //Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
         //       " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Face Tracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                //Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face authen handle
     */
    private boolean doAuthenticationEigen(boolean combinemode) {
        boolean eigenResult;
        Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_eigenface),
                "20"));

//        Float threshold2 = Float.parseFloat(pref.getString());

        String pathSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_EIGENFACE;
        String sketchSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_SKETCH_EIGENFACE;
        String hashSave = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_HASH_EIGENFACE;
        String mainSave = PhotoHandle.getSaveDir().getPath() + File.separator + Constant.XML_EIGEN_MAIN;


        if(prefManager.isSecurity()) {
                Toast.makeText(getApplication(),"CODEBOOK",Toast.LENGTH_SHORT).show();
                Eigenface eigen = StoreModel.readEigenface(pathSave, 30, threshold);
                Vector<Vector> sketch = StoreModel.readSketch(sketchSave, 30);
                Vector<Vector> projectHash = StoreModel.readHash(hashSave, 30);

                String photoSave = "1-0.jpg";
                PhotoAuthenticationHandle handle = new PhotoAuthenticationHandle(this, photoSave);

                mCameraSource.takePicture(null, handle );

                // Chup khuon mat hien tai
                String url = PhotoHandle.getDir().getPath() + File.separator + photoSave;
                File tmp = new File(url);
                if(!tmp.exists()) return false;
                Mat readImage = ContrastImage(imread(url));
                Mat authenMat = eigen.getAuthenMat(readImage);


                Vector<Vector> recover = Codebook.codebookRecover(authenMat, sketch);

                //Show vector dac trung recover
        //        for(int i = 0; i< recover.size(); i++){
        //            Log.e("TEST","vector recover eigen"+ i +": "+ recover.get(i));
        //        }

                Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
                try {
                    sketchHash = HelpMethod.hashTemplate(recover);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (SignatureException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }

                //Log.e("RES", String.valueOf(threshold));
                int result = Codebook.predict(sketchHash, projectHash, Constant.EIGENGFACE, threshold);
                eigenResult = (result == 1);
                if(!combinemode) HelpMethod.deleteTempImages() ;

                Log.e("Result", "Result EIGEN:" + String.valueOf(result));
        }
        else {
            Eigenface eigen = StoreModel.readEigenface(pathSave, mainSave, 30,PCA_THRESHOLD);
            String photoSave = "1-0.jpg";
            PhotoAuthenticationHandle handle = new PhotoAuthenticationHandle(this, photoSave);

            mCameraSource.takePicture(null, handle );

            // Chup khuon mat hien tai
            String url = PhotoHandle.getDir().getPath() + File.separator + photoSave;
            File tmp = new File(url);
            if(!tmp.exists()) return false;
            Mat readImage = ContrastImage(imread(url));

            //Log.e("RES", String.valueOf(threshold));
            int result = eigen.predictEigenface(readImage);
            eigenResult = (result == 1);
            if(!combinemode) HelpMethod.deleteTempImages() ;

            Log.e("Result", "Result EIGEN:" + String.valueOf(result));
        }
        return eigenResult;
    }

    private boolean doAuthenticationFisher(boolean combineMode){
        boolean fisherResult ;
        Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_fisherface),
                "4"));
        String pathSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_FISHERFACE;
        String sketchSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_SKETCH_FISHERFACE;
        String hashSave = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_HASH_FISHERFACE;
        String mainSave = PhotoHandle.getSaveDir().getPath() + File.separator + Constant.XML_FISHER_MAIN;


        if(prefManager.isSecurity()) {
            Fisherface fisher = StoreModel.readFisherface(pathSave, 30, threshold);
            Vector<Vector> sketch = StoreModel.readSketch(sketchSave, 9);
            Vector<Vector> projectHash = StoreModel.readHash(hashSave, 9);

            String photoSave = "1-0.jpg";

            if (!combineMode) {
                PhotoAuthenticationHandle handle = new PhotoAuthenticationHandle(this, photoSave);
                mCameraSource.takePicture(null, handle);

            }

            // Chup khuon mat hien tai
            String url = PhotoHandle.getDir().getPath() + File.separator + photoSave;
            File tmp = new File(url);
            if (!tmp.exists()) return false;

            Mat readImage = ContrastImage(imread(PhotoHandle.getDir().getPath() + File.separator + "1-0.jpg"));
            Mat authenMat = fisher.getAuthenMat(readImage);

            Vector<Vector> recover = Codebook.codebookRecover(authenMat, sketch);

            Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
            try {
                sketchHash = HelpMethod.hashTemplate(recover);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

            int result = Codebook.predict(sketchHash, projectHash, Constant.FISHERFACE, threshold);
            fisherResult = (result == 1);
            Log.e("Files", "Result FISHER" + String.valueOf(result));

            if (!combineMode) HelpMethod.deleteTempImages();
        } else {
            Fisherface fisher = StoreModel.readFisherface(pathSave, mainSave, 90, FLD_THRESHOLD);
            String photoSave = "1-0.jpg";

            if (!combineMode) {
                PhotoAuthenticationHandle handle = new PhotoAuthenticationHandle(this, photoSave);
                mCameraSource.takePicture(null, handle);

            }

            // Chup khuon mat hien tai
            String url = PhotoHandle.getDir().getPath() + File.separator + photoSave;
            File tmp = new File(url);
            if (!tmp.exists()) return false;

            Mat readImage = ContrastImage(imread(PhotoHandle.getDir().getPath() + File.separator + "1-0.jpg"));

            int result = fisher.predict(readImage);
            fisherResult = (result == 1);
            Log.e("Files", "Result FISHER" + String.valueOf(result));
            if (!combineMode) HelpMethod.deleteTempImages();
        }
        return fisherResult;
    }

    private boolean doAuthenticationLBP(boolean combineMode){
        boolean lbpResult ;

        String mainSave = PhotoHandle.getSaveDir().getPath() + File.separator + Constant.XML_LBP_MAIN;
        String sketchSave =   PhotoHandle.getDir().getPath() + File.separator + Constant.XML_SKETCH_LBP;
        String hashSave = PhotoHandle.getDir().getPath() + File.separator + Constant.XML_HASH_LBP;

        Float threshold = Float.parseFloat(pref.getString(getString(R.string.pref_threshold_key_lbp),
                "120"));

//        final LBP lbpSave = StoreModel.readLBP(projectSave);

        if(prefManager.isSecurity()) {
            final Vector<Vector> sketch = StoreModel.readSketch(sketchSave, 256);
            final Vector<Vector> projectHash = StoreModel.readHash(hashSave, 256);

            String photoSave = "1-0.jpg";

            if(!combineMode){
                PhotoAuthenticationHandle handle = new PhotoAuthenticationHandle(this, photoSave);
                mCameraSource.takePicture(null, handle );
            }

            // Chup khuon mat hien tai
            String url = PhotoHandle.getDir().getPath() + File.separator + photoSave;
            File tmp = new File(url);
            if(!tmp.exists()) return false;

            Mat readImage = ContrastImage(imread(PhotoHandle.getDir().getPath() + File.separator + "1-0.jpg"));
            int[] inputHist = LBP.getAuthenHist(readImage);
            Mat hist = new Mat(256, 1, CV_32SC1);
            IntBuffer histBuf = hist.createBuffer();
            for (int i = 0; i < 256; i++) {
                histBuf.put(i, inputHist[i]);
            }

            Vector<Vector> recover = Codebook.codebookRecover(hist, sketch);
            Vector<Vector> sketchHash = new Vector<Vector>(recover.size());
            try {
                sketchHash = HelpMethod.hashTemplate(recover);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

            int result = Codebook.predict(sketchHash, projectHash, Constant.LBPH, threshold);
            lbpResult = (result==1);

            Log.e("Files", "Result LBP " + String.valueOf(result));

            HelpMethod.deleteTempImages() ;
        }
        else {
            LBP lbp = StoreModel.readLBP(mainSave);
            String photoSave = "1-0.jpg";
            PhotoAuthenticationHandle handle = new PhotoAuthenticationHandle(this, photoSave);

            mCameraSource.takePicture(null, handle );

            // Chup khuon mat hien tai
            String url = PhotoHandle.getDir().getPath() + File.separator + photoSave;
            File tmp = new File(url);
            if(!tmp.exists()) return false;
            Mat readImage = ContrastImage(imread(url));

            //Log.e("RES", String.valueOf(threshold));
            int result = lbp.predict(readImage,LBP_THRESHOLD);
            lbpResult = (result == 1);
            HelpMethod.deleteTempImages() ;

            Log.e("Result", "Result LBP:" + String.valueOf(result));
        }


        return lbpResult;
    }

    private void showDialogWithConfirm() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Login fail")
                .setContentText("Do you want to continue ?")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        runAutoDetect();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        adminComponent = new ComponentName(getBaseContext(),
                                ReceiverClass.class);
                        // get system service.
                        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

                        // request admin role.
                        if (!devicePolicyManager.isAdminActive(adminComponent)) {
                            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminComponent);
                            startActivityForResult(intent, REQUEST_ENABLE);
                            //khong duoc de lock o day
                            sweetAlertDialog.dismiss();
                        } else {
                            // lock your phone.
                            sweetAlertDialog.dismiss();
                            devicePolicyManager.lockNow();
                        }

                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE) {
            if (resultCode == RESULT_OK) {
                Log.e("User accept!",".");
                devicePolicyManager.lockNow();
                finish();
            } else {
                Log.e("User not accept!",".");
                runAutoDetect();
            }
        } else {
            Log.e("Not request to admin!",".");
//            showToast("User do not accept!");
        }
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
            myFace = face;
            /*
            if(face != null) {
                facestat++;
                myFace = face;
            }
            boolean result = false;
            if(myFace != null && facestat > 10) {
                facestat= 0;
                //Log.e("Files", "Take photo");
                switch (algorithm){
                    case Constant.EIGENGFACE:
                        result = doAuthenticationEigenface();
                        if (result)
                        {
                            Intent resultIntent = new Intent();
                            setResult(Activity.RESULT_OK, resultIntent);

                            finish();
                        }
                        virbate();
                        break;
                    case Constant.FISHERFACE:
                        result = doAuthenticationFisher();
                        if (result)
                        {
                            Intent resultIntent = new Intent();
                            setResult(Activity.RESULT_OK, resultIntent);
                            virbate();
                            finish();
                        }

                        break;
                    case Constant.LBPH:
                        result = doAuthenticationLBP();
                        if (result)
                        {
                            Intent resultIntent = new Intent();
                            setResult(Activity.RESULT_OK, resultIntent);
                            virbate();
                            finish();
                        }
                        break;
                    case Constant.ALL:
                        break;
                    default: break;
                }
            }*/
        }

        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }
}
